<?php

namespace Drupal\contacts_events;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Ticket entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class TicketHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($transfer_page_route = $this->getTransferPageRoute($entity_type)) {
      $collection->add("entity.{$entity_type->id()}.transfer_form", $transfer_page_route);
    }

    return $collection;
  }

  /**
   * Gets the transfer page route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getTransferPageRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->id() == 'contacts_ticket' && $entity_type->hasLinkTemplate('transfer-form')) {
      $route = new Route($entity_type->getLinkTemplate('transfer-form'));
      $route->setDefault('_entity_form', $entity_type->id() . '.transfer');
      $route->setDefault('_title', 'Transfer ticket');
      $route->setDefault('entity_type_id', $entity_type->id());
      $route->setOption('_admin_route', TRUE);
      $route->setOption('parameters', [
        'contacts_ticket' => ['type' => 'entity:contacts_ticket'],
      ]);
      $route->setRequirement('_entity_access', $entity_type->id() . '.transfer');

      return $route;
    }

    return NULL;
  }

}

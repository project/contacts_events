<?php

namespace Drupal\contacts_events;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;

/**
 * Trait for common order state checks and processes.
 */
trait OrderStateTrait {

  /**
   * Check if an order has unconfirmed items.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check.
   *
   * @return bool
   *   Whether there are unconfirmed items.
   */
  protected function orderHasUnconfirmedItems(OrderInterface $order) {
    foreach ($order->getItems() as $item) {
      if ($item->hasField('state') && !$item->get('state')->isEmpty()) {
        /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $item_state */
        $item_state = $item->get('state')->first();
        $item_transitions = $item_state->getWorkflow()
          ->getPossibleTransitions($item_state->value);
        if (isset($item_transitions['confirm'])) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Applies transition if it's allowed.
   *
   * @param \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state
   *   State field.
   * @param string $transition_id
   *   The ID of the transition to invoke.
   * @param bool $save
   *   Whether to save the entity (default FALSE).
   *
   * @return bool
   *   Whether a transition was applied.
   */
  protected function applyTransitionIfAllowed(StateItemInterface $state, $transition_id, $save = FALSE): bool {
    $entity = $state->getEntity();
    $transitions = $state->getWorkflow()->getAllowedTransitions($state->value, $entity);

    if (!isset($transitions[$transition_id])) {
      return FALSE;
    }

    $state->applyTransition($transitions[$transition_id]);

    if ($save) {
      $entity->save();
    }

    return TRUE;
  }

  /**
   * Find transitions that have a certain stub at the end of the ID.
   *
   * @param \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state
   *   State field.
   * @param string $transition_stub
   *   The substring for transition IDs to look for.
   *
   * @return string|null
   *   The transition ID or NULL if none found.
   *
   * @todo This needs test coverage.
   */
  protected function findAllowedTransitionFromStub(StateItemInterface $state, $transition_stub) {
    $entity = $state->getEntity();
    $transitions = $state->getWorkflow()->getAllowedTransitions($state->value, $entity);

    foreach (array_keys($transitions) as $transition_id) {
      if (substr($transition_id, -(strlen($transition_stub))) == $transition_stub) {
        return $transition_id;
      }
    }
  }

  /**
   * Check whether the paid amount has changed.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $original
   *   The original, if any.
   *
   * @return bool
   *   Whether the amount paid has changed.
   */
  protected function hasPaidChanged(OrderInterface $order, ?OrderInterface $original = NULL): bool {
    $paid = $order->getTotalPaid();
    $original_paid = $original ? $original->getTotalPaid() : NULL;

    // If we don't have either, there's no change.
    if (!$paid && !$original_paid) {
      return FALSE;
    }

    // If we only have one and the other is non-zero, it has changed.
    if ((!$paid && !$original_paid->isZero()) || (!$original_paid && !$paid->isZero())) {
      return TRUE;
    }

    // If we only have one, and the other is zero, it hasn't changed.
    if ((!$paid && $original_paid->isZero()) || (!$original_paid && $paid->isZero())) {
      return FALSE;
    }

    // Otherwise check for equality.
    return !$paid->equals($original_paid);
  }

}

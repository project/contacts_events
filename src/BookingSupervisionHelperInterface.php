<?php

namespace Drupal\contacts_events;

/**
 * Provides an interface for booking supervision helpers.
 */
interface BookingSupervisionHelperInterface {

  /**
   * Get the number of adult delegates.
   *
   * @return int
   *   The number of adult delegates.
   */
  public function getAdultDelegates() : int;

  /**
   * Get the number of non adult delegates.
   *
   * @return int
   *   The number of non adult delegates.
   */
  public function getNonAdultDelegates() : int;

}

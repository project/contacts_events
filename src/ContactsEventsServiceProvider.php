<?php

namespace Drupal\contacts_events;

use Drupal\contacts_events\EventSubscriber\StripeIntentOrderSubscriber;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Custom service provider for Contacts Events.
 */
class ContactsEventsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['bookkeeping'])) {
      $container->register('contacts_events.entity_hooks.commerce_order_item.bookkeeping', 'Drupal\contacts_events\Entity\OrderItemBookkeepingHooks')
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('config.factory'))
        ->addArgument(new Reference('event_dispatcher'));

      $container->register('contacts_events.bookkeeping_transaction_subscriber', 'Drupal\contacts_events\EventSubscriber\TransactionSubscriber')
        ->addTag('event_subscriber');
    }

    if (isset($modules['contacts_references']) && isset($modules['contacts_events_teams'])) {
      $container->register('contacts_events.reference_subscriber', 'Drupal\contacts_events_teams\EventSubscriber\ReferenceSubscriber')
        ->addArgument(new Reference('contacts_references.reference_field_helper'))
        ->addArgument(new Reference('contacts_references.sender'))
        ->addArgument(new Reference('entity_type.manager'))
        ->addTag('event_subscriber');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Replace the Commerce Checkout Order Manager with our custom version.
    $definition = $container->getDefinition('commerce_checkout.checkout_order_manager');
    $definition->setClass('Drupal\contacts_events\CustomCheckoutOrderManager');

    // Swap out the Stripe intent updater for one that's aware of partial
    // payments.
    if ($container->hasDefinition('commerce_stripe.order_events_subscriber')) {
      $definition = $container->getDefinition('commerce_stripe.order_events_subscriber');
      $definition->setClass(StripeIntentOrderSubscriber::class);
      $definition->addMethodCall('setPrivateTempstore', [new Reference('tempstore.private')]);
    }
  }

}

<?php

namespace Drupal\contacts_events\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The event class form.
 */
class EventClassForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\contacts_events\Entity\EventClassInterface $class */
    $class = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $class->label(),
      '#description' => $this->t("Label for the Class."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $class->id(),
      '#machine_name' => [
        'exists' => '\Drupal\contacts_events\Entity\EventClass::load',
      ],
      '#disabled' => !$class->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $class->status(),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Context'),
      '#empty_option' => $this->t('Global'),
      '#empty_value' => 'global',
      '#default_value' => $class->get('type'),
      '#options' => [],
    ];

    $order_item_types = $this->entityTypeManager
      ->getStorage('commerce_order_item_type')
      ->loadMultiple();
    foreach ($order_item_types as $type) {
      $form['type']['#options'][$type->id()] = $type->label();
    }

    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => $class->get('weight') ?? 0,
      '#delta' => 20,
    ];

    $form['selectable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This class is selectable'),
      '#description' => $this->t('If an item has multiple selectable types, the user will be presented with a choice.'),
      '#default_value' => $class->get('selectable'),
    ];

    $form['min_age'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum Age'),
      '#description' => $this->t('Minimum age allowed for age calculation. This is inclusive.'),
      '#default_value' => $class->get('min_age'),
    ];

    $form['max_age'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Age'),
      '#description' => $this->t('Maximum age allowed for age calculation. This is inclusive.'),
      '#default_value' => $class->get('max_age'),
    ];

    $form['bypass_age_check_on_empty_dob'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass age check when no date of birth specified'),
      '#description' => $this->t('When checked and a ticket holder has no date of birth specified, the age range will be skipped and the class will be allowed'),
      '#default_value' => $class->get('bypass_age_check_on_empty_dob'),
    ];

    // Min/Max age fields should only be available if the slected context
    // is contacts_ticket.
    $form['max_age']['#states']['visible'][] = [
      ':input[name="type"]' => ['value' => 'contacts_ticket'],
    ];
    $form['min_age']['#states']['visible'][] = [
      ':input[name="type"]' => ['value' => 'contacts_ticket'],
    ];
    $form['bypass_age_check_on_empty_dob']['#states']['visible'][] = [
      ':input[name="type"]' => ['value' => 'contacts_ticket'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);

    // FormBuilder uses the submitted value if it gets a NULL response from a
    // #value_callback, but we want NULL, not an empty string.
    if ($form_state->getValue('min_age') == '') {
      $entity->set('min_age', NULL);
    }

    if ($form_state->getValue('max_age') == '') {
      $entity->set('max_age', NULL);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $contacts_events_class = $this->entity;
    $status = $contacts_events_class->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Class.', [
          '%label' => $contacts_events_class->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Class.', [
          '%label' => $contacts_events_class->label(),
        ]));
    }
    $form_state->setRedirectUrl($contacts_events_class->toUrl('collection'));
  }

}

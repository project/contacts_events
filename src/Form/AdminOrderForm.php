<?php

namespace Drupal\contacts_events\Form;

use Drupal\commerce_order\Form\OrderForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Override OrderForm to redirect to the edit page and set the checkout step.
 */
class AdminOrderForm extends OrderForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if ($this->getOperation() == 'booking_admin_add') {
      $this->entity->set('checkout_step', 'tickets');
    }

    parent::save($form, $form_state);

    $form_state->setRedirect('entity.commerce_order.edit_form', ['commerce_order' => $this->entity->id()]);
  }

}

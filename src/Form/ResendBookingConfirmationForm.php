<?php

namespace Drupal\contacts_events\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Mail\OrderReceiptMailInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form that resends booking confirmation.
 *
 * @package Drupal\contacts_events\Form
 */
class ResendBookingConfirmationForm extends FormBase {

  /**
   * Mail sender.
   *
   * @var \Drupal\commerce_order\Mail\OrderReceiptMailInterface
   */
  protected $mail;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(OrderReceiptMailInterface $mail, EntityTypeManagerInterface $entity_type_manager) {
    $this->mail = $mail;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_order.order_receipt_mail'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_events_resend_confirmation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Resend Booking Confirmation'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $args = $form_state->getBuildInfo()['args'];

    if (isset($args[0]) && $args[0] instanceof Order) {
      $order = $args[0];
      /** @var \Drupal\commerce_order\Entity\OrderType $order_type */
      $order_type = $this->entityTypeManager->getStorage('commerce_order_type')->load($order->bundle());
      if ($order_type->shouldSendReceipt()) {
        $this->mail->send($order);
        $this->messenger()->addMessage($this->t('Booking confirmation sent to @mail', ['@mail' => $order->getEmail()]));
      }
    }
  }

}

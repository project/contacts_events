<?php

namespace Drupal\contacts_events\Form;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * The order item cancel form.
 *
 * @package Drupal\contacts_events\Form
 */
class OrderItemCancelForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $message = $this->t('Are you sure you want to cancel %label?', ['%label' => $this->entity->label()]);

    $form['message'] = [
      '#theme_wrappers' => ['container'],
      '#markup' => $message,
    ];

    $form = parent::form($form, $form_state);

    $form['purchased_entity']['#access'] = $this->currentUser()->hasPermission('can manage bookings for contacts_events');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processForm($element, FormStateInterface $form_state, $form) {
    $element = parent::processForm($element, $form_state, $form);

    if (!empty($element['actions']['submit'])) {
      $element['actions']['submit']['#value'] = $this->t('Cancel ticket');
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    if (!$this->entity instanceof OrderItemInterface) {
      return;
    }

    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->entity->get('state')->first();
    $state->applyTransitionById('cancel');

    /** @var \Drupal\contacts_events\Entity\Ticket $ticket */
    $ticket = $this->entity->getPurchasedEntity();

    if (!$this->entity->get('mapped_price')->isEmpty()) {
      $mapped_price = $this->entity->get('mapped_price')->first()->getValue();
      $mapped_price['class_overridden'] = TRUE;
      $this->entity->set('mapped_price', $mapped_price);
      $ticket->setMappedPrice($mapped_price);
    }
    $this->entity->save();

    // Clear contact and email so they could be re-booked on to event.
    $ticket->set('contact', NULL);
    $ticket->set('email', NULL);
    $ticket->save();
  }

}

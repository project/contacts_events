<?php

namespace Drupal\contacts_events\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Event entities.
 *
 * @ingroup contacts_events
 */
class EventDeleteForm extends ContentEntityDeleteForm {

}

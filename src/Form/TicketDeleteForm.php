<?php

namespace Drupal\contacts_events\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ticket entities.
 *
 * @ingroup contacts_events
 */
class TicketDeleteForm extends ContentEntityDeleteForm {


}

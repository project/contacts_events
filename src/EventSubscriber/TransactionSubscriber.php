<?php

namespace Drupal\contacts_events\EventSubscriber;

use Drupal\bookkeeping\Event\OrderTransactionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber for transaction posting events.
 */
class TransactionSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderTransactionEvent::EVENT] = ['excludeStatefulOrderItems'];
    return $events;
  }

  /**
   * Exclude stateful order items from the values for order transactions.
   *
   * Stateful order items are posted directly in response to their state
   * being changed.
   *
   * @param \Drupal\bookkeeping\Event\OrderTransactionEvent $event
   *   The transaction event.
   */
  public function excludeStatefulOrderItems(OrderTransactionEvent $event) {
    $order = $event->getOrder();
    if ($order->bundle() === 'contacts_booking') {
      // @todo This will NOT work if we add non stateless order items to
      // bookings. We don't currently have this scenario in place, so this is
      // fine for now.
      $event->prevent();
    }
  }

}

<?php

namespace Drupal\contacts_events\Cron;

use Drupal\Component\Datetime\Time;
use Drupal\contacts_events\PriceCalculator;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Cron task for triggering price recalculations on booking window changes.
 */
class RecalculateOnBookingWindow extends BookingWindowCronBase {

  /**
   * The state key for tracking the last run of the price recalculation.
   */
  const STATE_LAST_RUN = 'contacts_events.price_recalculation.last_run';

  /**
   * The price calculator service.
   *
   * @var \Drupal\contacts_events\PriceCalculator
   */
  protected $priceCalculator;

  /**
   * Construct the price recalculation on booking window cron service.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\Time $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\contacts_events\PriceCalculator $price_calculator
   *   The price calculator service.
   *
   * @throws \Exception
   *   Thrown if the STATE_LAST_RUN constant is not set.
   */
  public function __construct(StateInterface $state, Time $time, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, PriceCalculator $price_calculator) {
    parent::__construct($state, $time, $entity_type_manager, $entity_field_manager);
    $this->priceCalculator = $price_calculator;
  }

  /**
   * {@inheritdoc}
   */
  protected function runActionsForEvents($event_ids, $order_item_types): void {
    $this->priceCalculator->enqueueJobs($event_ids, $order_item_types);
  }

}

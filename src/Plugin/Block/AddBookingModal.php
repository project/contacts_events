<?php

namespace Drupal\contacts_events\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a block to create a new booking in a modal.
 *
 * @Block(
 *   id = "contacts_events_add_booking_modal",
 *   admin_label = @Translation("Add Booking (modal)"),
 *   category = @Translation("Inline blocks"),
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user", required = TRUE, label = @Translation("User"))
 *   }
 * )
 */
class AddBookingModal extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getContextValue('user');
    $block['open_modal'] = [
      '#type' => 'link',
      '#title' => $this->t('+ Add Booking'),
      '#url' => Url::fromRoute('contacts_events.entity.commerce_order.add_page.admin', ['user' => $user->id()]),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
        'data-dialog-type' => ['modal'],
      ],
      '#attached' => [
        'library' => ['core/drupal.dialog.ajax'],
      ],
    ];

    return $block;
  }

}

<?php

namespace Drupal\contacts_events\Plugin\views\field;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to the booking process.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("contacts_events_booking_links")
 */
class BookingLink extends EntityLink {

  /**
   * {@inheritdoc}
   */
  protected function getEntityLinkTemplate() {
    return 'booking_process';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Continue');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkUrlAccess(ResultRow $row) {
    $entity = $this->getEntity($row);
    if ($entity && $entity->bundle() != 'contacts_booking') {
      return AccessResult::forbidden('Order is not a booking.')
        ->addCacheableDependency($entity);
    }
    return parent::checkUrlAccess($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    if (!empty($this->options['continue_draft'])) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $booking */
      $booking = $this->getEntity($row);

      /** @var \Drupal\commerce_checkout\CheckoutOrderManager $checkout_manager */
      $checkout_manager = \Drupal::service('commerce_checkout.checkout_order_manager');
      /** @var \Drupal\contacts_events\Plugin\Commerce\CheckoutFlow\BookingFlowInterface $flow */
      $flow = $checkout_manager->getCheckoutFlow($booking)->getPlugin();
      $flow->setOrder($booking);
      $url = $flow->getContinueUrl()->setAbsolute($this->options['absolute']);
    }
    else {
      $url = parent::getUrlInfo($row);
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['continue_draft'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['continue_draft'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Continue booking if in %draft state', [
        '%draft' => $this->t('Started'),
      ]),
      '#description' => $this->t("If enabled, the user will be taken into the booking process instead of the booking dashboard for bookings which haven't completed the checkout step at least once."),
      '#default_value' => $this->options['continue_draft'],
    ];
  }

}

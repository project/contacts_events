<?php

namespace Drupal\contacts_events\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides Booking relation icons field handler.
 *
 * @ViewsField("contacts_events_booking_relation_icons")
 */
class BookingRelationIcons extends BookingInfoBase {

  /**
   * {@inheritdoc}
   */
  protected function renderBooking(OrderInterface $booking, $uid) {
    $icons = [];

    // Check if we're the booking manager.
    if ($booking->getCustomerId() == $uid) {
      $icons[] = [
        '#type' => 'open_iconic',
        '#icon' => 'cart',
        '#title' => $this->t('Booking manager'),
        '#color' => NULL,
        '#fill' => '#000',
        '#size' => '1em',
      ];
    }

    // Check if we are a ticket holder.
    foreach ($booking->getItems() as $item) {
      if ($item->bundle() != 'contacts_ticket') {
        continue;
      }

      /** @var \Drupal\contacts_events\Entity\TicketInterface $ticket */
      $ticket = $item->getPurchasedEntity();
      if ($ticket->get('contact')->target_id == $uid) {
        $icons[] = [
          '#type' => 'open_iconic',
          '#icon' => 'person',
          '#title' => $this->t('Ticket holder'),
          '#color' => NULL,
          '#fill' => '#000',
          '#size' => '1em',
        ];
        break;
      }
    }

    return $icons;
  }

}

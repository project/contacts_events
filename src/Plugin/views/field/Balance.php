<?php

namespace Drupal\contacts_events\Plugin\views\field;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Order balance views field hander.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("contacts_events_balance")
 */
class Balance extends FieldPluginBase {

  /**
   * The currency formatter service.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * Constructs the Balance views field plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrencyFormatterInterface $currency_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_price.currency_formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->getEntity($values);
    $price = $order->getBalance();

    if (empty($price)) {
      return NULL;
    }

    return $this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode());
  }

}

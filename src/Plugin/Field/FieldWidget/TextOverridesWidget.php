<?php

namespace Drupal\contacts_events\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'contacts_events_text_overrides' field widget.
 *
 * @FieldWidget(
 *   id = "contacts_events_text_overrides",
 *   label = @Translation("Text overrides"),
 *   field_types = {"contacts_events_text_overrides"},
 * )
 */
class TextOverridesWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    // Ensure all the relevant items exist.
    $existing = [];
    foreach ($items as $item) {
      $existing[] = $item->name;
    }

    foreach ($this->fieldDefinition->getFieldStorageDefinition()->getSetting('allowed_keys') as $key => $default) {
      if (!in_array($key, $existing)) {
        $items->appendItem([
          'name' => $key,
        ]);
      }
    }

    $field_state = static::getWidgetState($form['#parents'], $this->fieldDefinition->getName(), $form_state);
    $field_state['items_count'] = $items->count() - 1;
    static::setWidgetState($form['#parents'], $this->fieldDefinition->getName(), $form_state, $field_state);

    $element = parent::formMultipleElements($items, $form, $form_state);

    $element['#cardinality'] = $field_state['items_count'];
    $element['#multiple'] = FALSE;
    $element['#theme'] = NULL;
    unset($element['add_more']);
    foreach (Element::children($element) as $key) {
      $element[$key]['_weight']['#type'] = 'value';
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $labels = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('allowed_keys');

    $name = $items[$delta]->name;
    $element['name'] = [
      '#type' => 'value',
      '#value' => $name,
    ];

    $element['text'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('%label link text', [
        '%label' => $labels[$name] ?? $name,
      ]),
      '#description' => new TranslatableMarkup('Text for the %label link. Leave empty to use the default.', [
        '%label' => $labels[$name] ?? $name,
      ]),
      '#placeholder' => $labels[$name] ?? NULL,
      '#default_value' => $items[$delta]->text ?? NULL,
      '#size' => 20,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if ($value['text'] === '') {
        $values[$delta]['text'] = NULL;
      }
    }
    return $values;
  }

}

<?php

namespace Drupal\contacts_events\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Formats datetime as age calculated against start of event.
 *
 * @FieldFormatter(
 *   id = "datetime_age",
 *   label = @Translation("Age at event"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class AgeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\contacts_events\Entity\TicketInterface $entity */
    $entity = $items->getEntity();
    $event = $entity->getEvent();
    /** @var \Drupal\Core\Datetime\DrupalDateTime $event_start */
    $event_start = $event->get('date')->start_date ?? new DrupalDateTime();

    foreach ($items as $delta => $item) {
      if ($item->value) {
        $elements[$delta] = [
          '#markup' => $item->date->diff($event_start)->y,
        ];
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() == 'contacts_ticket'
      && $field_definition->getFieldStorageDefinition()->getCardinality() == 1;
  }

}

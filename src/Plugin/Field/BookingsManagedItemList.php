<?php

namespace Drupal\contacts_events\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\user\UserInterface;

/**
 * Computed item list for the bookings managed by a user.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class BookingsManagedItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $user = $this->getEntity();
    if (!$user instanceof UserInterface) {
      throw new \Exception('BookingsManagedItemList is only suitable for fields on the user entity.');
    }

    // Instantiate the list and do nothing for anonymous users.
    $this->list = [];
    if ($user->isAnonymous()) {
      return;
    }

    $ids = \Drupal::entityQuery('commerce_order')
      ->accessCheck(FALSE)
      ->condition('type', 'contacts_booking')
      ->condition('uid', $user->id())
      ->execute();

    $delta = 0;
    foreach ($ids as $id) {
      $this->list[$delta] = $this->createItem($delta, $id);
      $delta++;
    }
  }

}

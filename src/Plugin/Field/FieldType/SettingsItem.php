<?php

namespace Drupal\contacts_events\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Defines the 'contacts_events_settings' field type.
 *
 * @FieldType(
 *   id = "contacts_events_settings",
 *   label = @Translation("Serialized settings field"),
 *   description = @Translation("A field containing a serialized string value."),
 *   category = @Translation("Events"),
 *   list_class = "\Drupal\contacts_events\Plugin\Field\SettingsItemList",
 *   cardinality = 1,
 * )
 */
class SettingsItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $map_definition = MapDataDefinition::create()
      ->setLabel(new TranslatableMarkup('Serialized value'));

    $property_definitions = $field_definition->getSetting('properties') ?? [];
    foreach ($property_definitions as $property_name => $property_configuration) {
      $map_definition->setPropertyDefinition($property_name, self::recursivelyBuildDefinition($property_configuration));
    }

    $properties['value'] = $map_definition;
    return $properties;
  }

  /**
   * Recursively build property definitions from configuration.
   *
   * @param array|string $configuration
   *   Either an array, indicating a map with sub properties, or a data type.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface
   *   The data definition.
   */
  protected static function recursivelyBuildDefinition($configuration): DataDefinitionInterface {
    if (is_array($configuration)) {
      $definition = MapDataDefinition::create();
      foreach ($configuration as $property_name => $property_configuration) {
        $definition->setPropertyDefinition($property_name, self::recursivelyBuildDefinition($property_configuration));
      }
    }
    else {
      $definition = DataDefinition::create($configuration);
    }
    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->value);
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Ensure we always set an array.
    $values ?: [];

    if (!isset($values['value'])) {
      $values = ['value' => $values];
    }

    parent::setValue($values, $notify);
  }

}

<?php

namespace Drupal\contacts_events\Plugin\Field\FieldType;

use Drupal\contacts_events\Entity\EventClass;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Represents the booking windows field.
 */
class PriceMapItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItem[]
   */
  protected $list = [];

  /**
   * The price map.
   *
   * @var \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItem[][]
   *
   * @see \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItemList::getPriceMap
   */
  protected $map;

  /**
   * Build an associative array containing the price mapping.
   *
   * @return \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItem[][]
   *   An array with outer keys of booking window ID and class ID. The delta for
   *   each item is retrievable by calling $item->getName().
   */
  public function getPriceMap() {
    if (!isset($this->map)) {
      $this->map = [];
      foreach ($this->list as $delta => $item) {
        $this->map[$item->getBookingWindow()][$item->getClass()] = $item;
      }
    }
    return $this->map;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * Get the booking windows for this price map.
   *
   * @return \Drupal\contacts_events\Plugin\Field\FieldType\BookingWindowsItemList|null
   *   The booking windows or NULL if we can't get any.
   */
  public function getBookingWindows() {
    $field_name = $this->getFieldDefinition()->getSetting('booking_window_field');

    if ($items = $this->getFieldItems($field_name)) {
      $items->filterEmptyItems();
    }

    return $items;
  }

  /**
   * Get the classes for this price map.
   *
   * @return \Drupal\contacts_events\Entity\EventClassInterface[]
   *   The sorted classes.
   */
  public function getClasses() {
    $field_name = $this->getFieldDefinition()->getSetting('class_field');
    $items = $this->getFieldItems($field_name);
    if (!$items) {
      return [];
    }

    $classes = $items->filterEmptyItems()->referencedEntities();
    uasort($classes, [EventClass::class, 'sort']);
    return $classes;
  }

  /**
   * Get the field items.
   *
   * @param string $field_name
   *   The field name we are retrieving. You can retrieve fields on referenced
   *   entities by splitting with a '.'. For example 'event.ticket_price' will
   *   get the 'ticket_price' field on the entity referenced in the 'event'
   *   field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field items being retrieved or NULL if something is broken down the
   *   chain.
   */
  protected function getFieldItems($field_name): ?FieldItemListInterface {
    $entity = $this->getEntity();
    if (!$field_name || !$entity) {
      return NULL;
    }

    // Break out nested ER references.
    $entities = explode('.', $field_name);
    $field_name = array_pop($entities);

    // Track through referenced entities, if any, to get the final entity.
    foreach ($entities as $entity_field_name) {
      // Check the field exists.
      if (!$entity->hasField($entity_field_name)) {
        return NULL;
      }

      $field = $entity->get($entity_field_name);

      // Ensure this is an entity reference.
      if (!($field instanceof EntityReferenceFieldItemListInterface)) {
        return NULL;
      }

      // Get the entity.
      $entity = $field->entity;

      // Check the entity is loaded successfully.
      if (!$entity) {
        return NULL;
      }
    }

    // Return our eventual field items.
    return $entity && $entity->hasField($field_name) ?
      $entity->get($field_name)->filterEmptyItems() :
      NULL;
  }

}

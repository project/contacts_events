<?php

namespace Drupal\contacts_events\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Validates that a field is unique for tickets at the same event.
 */
class UniqueFieldValueForEventTicketValidator extends UniqueFieldValueForGivenReferenceFieldValidator {

  /**
   * {@inheritdoc}
   */
  protected function alterQuery(QueryInterface $query) {
    $ignored_states = [
      'cancelled',
    ];
    $query->condition('order_item.entity.state', $ignored_states, 'NOT IN');
  }

}

<?php

namespace Drupal\contacts_events\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that a booking window is unique.
 */
class BookingWindowsUniqueConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\contacts_events\Plugin\Field\FieldType\BookingWindowsItemList $items */
    // Track existing labels and cut offs.
    $properties = [
      'id' => 'messageId',
      'label' => 'messageLabel',
      'cut_off' => 'messageCutOff',
      'cut_off_confirmed' => 'messageCutOff',
    ];
    $existing = array_fill_keys(array_keys($properties), []);
    $errors = [];

    // Loop over each item and property.
    foreach ($items as $item) {
      foreach ($properties as $property => $message) {
        $value = $item->{$property};

        // Note special handling of cutoff dates. If a window is set to have
        // a different confirmed date, then this should be used when comparing
        // against other windows' cutoff, as it's OK for 2 windows to have
        // the same cutoff date so long as the cut off confirmed date is
        // different.
        if ($property === 'cut_off' && $item->use_confirmed) {
          $value = $item->cut_off_confirmed;
        }

        // If it already exists, track for an error.
        if (in_array($value, $existing[$property])) {
          $errors[$message] = $message;
        }
        // Otherwise track the value.
        else {
          $existing[$property][] = $value;
        }
      }
    }

    foreach ($errors as $message) {
      $this->context->addViolation($constraint->{$message});
    }
  }

}

<?php

namespace Drupal\contacts_events\Plugin\Validation\Constraint;

/**
 * Checks if a field has a unique value for all tickets at the same event.
 *
 * @Constraint(
 *   id = "UniqueFieldValueForEventTicket",
 *   label = @Translation("Unique field for a ticket at the same event", context = "Validation"),
 * )
 */
class UniqueFieldValueForEventTicket extends UniqueFieldValueForGivenReferenceField {

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return '\Drupal\contacts_events\Plugin\Validation\Constraint\UniqueFieldValueForEventTicketValidator';
  }

}

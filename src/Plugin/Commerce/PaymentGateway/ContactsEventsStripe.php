<?php

namespace Drupal\contacts_events\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe\ErrorHelper;
use Drupal\commerce_stripe\Plugin\Commerce\PaymentGateway\Stripe;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override for the Stripe gateway to handle deposits/other partial payments.
 */
class ContactsEventsStripe extends Stripe {

  /**
   * The booking payment private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempstore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->tempstore = $container->get('tempstore.private')->get('contacts_events.booking_flow');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentIntent(OrderInterface $order, $capture = TRUE) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $order->get('payment_method')->entity;

    $payment_method_remote_id = $payment_method->getRemoteId();
    $customer_remote_id = $this->getRemoteCustomerId($order->getCustomer());

    $amount = $this->getPaymentAmount($order);
    $order_id = $order->id();
    $capture_method = $capture ? 'automatic' : 'manual';
    $intent_array = [
      'amount' => $this->toMinorUnits($amount),
      'currency' => strtolower($amount->getCurrencyCode()),
      'payment_method_types' => ['card'],
      'metadata' => [
        'order_id' => $order_id,
        'store_id' => $order->getStoreId(),
      ],
      'payment_method' => $payment_method_remote_id,
      'capture_method' => $capture_method,
    ];
    if (!empty($customer_remote_id)) {
      $intent_array['customer'] = $customer_remote_id;
    }
    try {
      $intent = PaymentIntent::create($intent_array);
      $order->setData('stripe_intent', $intent->id)->save();
    }
    catch (ApiErrorException $e) {
      ErrorHelper::handleException($e);
    }
    return $intent;
  }

  /**
   * Get the payment amount from the private tempstore.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment amount, or NULL if there is none specified.
   */
  protected function getPaymentAmount(OrderInterface $order): ?Price {
    $amount = $this->tempstore->get("{$order->id()}:payment_amount");
    return $amount ? Price::fromArray($amount) : $order->getBalance();
  }

}

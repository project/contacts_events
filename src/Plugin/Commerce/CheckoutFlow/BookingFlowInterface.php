<?php

namespace Drupal\contacts_events\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Url;

/**
 * Defines the interface for checkout flows for event bookings.
 */
interface BookingFlowInterface extends CheckoutFlowWithPanesInterface {

  /**
   * Sets the current order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The current order.
   *
   * @return $this
   */
  public function setOrder(OrderInterface $order);

  /**
   * Get the first step to start a new booking flow on.
   *
   * @return string
   *   The initial step.
   */
  public function getInitialStep(): string;

  /**
   * Get the url to continue the booking.
   *
   * @return \Drupal\Core\Url
   *   Url to the booking process.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getContinueUrl(): Url;

  /**
   * Whether there is anything to pay on the booking.
   *
   * @param bool $include_pending
   *   Whether to include pending payments.
   *
   * @return bool
   *   Whether there is anything to pay.
   */
  public function needsPayment(bool $include_pending = FALSE);

  /**
   * Get the pending payment amounts for the booking.
   *
   * @return \Drupal\commerce_price\Price[][]
   *   Price objects containing the amounts (in each currency code if multiple),
   *   outer keyed by payment gateway.
   */
  public function getPendingAmounts(): array;

  /**
   * Whether there is anything on the booking that needs confirming.
   *
   * @return bool
   *   Whether there is anything to confirm.
   */
  public function needsConfirmation();

}

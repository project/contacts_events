<?php

namespace Drupal\contacts_events;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Event entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class EventHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($booking_route = $this->getBookingRoute($entity_type)) {
      $collection->add("entity.$entity_type_id.book", $booking_route);
    }

    if ($clone_route = $this->getCloneRoute($entity_type)) {
      $collection->add("entity.$entity_type_id.clone", $clone_route);
    }

    return $collection;
  }

  /**
   * Gets the book now route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getBookingRoute(EntityTypeInterface $entity_type) {
    $route = new Route("/event/{contacts_event}/book");
    $route
      ->setDefaults([
        '_controller' => 'Drupal\contacts_events\Controller\EventController::book',
        '_title' => "Book Now",
      ])
      // Access controls are handled in the controller.
      ->setRequirement('_access', 'TRUE')
      // Make sure caching is definitely disabled so we don't get cached
      // redirect responses.
      ->setOption('no_cache', TRUE);

    // @todo As we create entities (albeit empty), let's protect from CSRF.
    // Currently not done due to token changes when logging in.
    return $route;
  }

  /**
   * Gets the event clone route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCloneRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('edit-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('clone-form'));
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.clone",
          '_title_callback' => 'Drupal\contacts_events\Form\EventCloneForm::title',
        ])
        // User must have both update access for this event and create access.
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setRequirement('_entity_create_access', 'contacts_event')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          $entity_type_id => ['type' => "entity:{$entity_type_id}"],
        ]);

      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }

      return $route;
    }
    return NULL;
  }

}

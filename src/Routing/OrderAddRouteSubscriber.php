<?php

namespace Drupal\contacts_events\Routing;

use Drupal\contacts_events\Form\BookingAddForm;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Modifies Add Order form when Booking is a selectable bundle.
 */
class OrderAddRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('entity.commerce_order.add_page');
    $route->setDefault('_form', BookingAddForm::class);
  }

}

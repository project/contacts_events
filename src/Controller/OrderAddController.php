<?php

namespace Drupal\contacts_events\Controller;

use Drupal\commerce_store\CurrentStore;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Element;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for adding orders for contacts.
 */
class OrderAddController extends ControllerBase {

  /**
   * The current store service.
   *
   * @var \Drupal\commerce_store\CurrentStore
   */
  protected $currentStore;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * Constructs a new EventController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_store\CurrentStore $current_store
   *   The current store service.
   * @param \Drupal\Core\Entity\EntityFormBuilder $form_builder
   *   The form builder service.
   */
  public function __construct(EntityTypeManager $entity_type_manager, CurrentStore $current_store, EntityFormBuilder $form_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentStore = $current_store;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('commerce_store.current_store'),
      $container->get('entity.form_builder')
    );
  }

  /**
   * Build and render an entity form for a new contacts_booking Order.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   The user the order should belong to.
   * @param string $form_mode
   *   The form mode to use.
   *
   * @return array
   *   Renderable form array.
   */
  public function build(UserInterface $user = NULL, $form_mode = 'booking_admin_add') {
    $order_values = [
      'type' => 'contacts_booking',
      'store_id' => $this->currentStore->getStore()->id(),
    ];

    $elements_to_show = [
      'event',
      'actions',
      'form_build_id',
      'form_token',
      'form_id',
    ];

    // Allow filling in of user if not provided.
    if (!empty($user)) {
      $order_values['uid'] = $user->id();
    }
    else {
      $elements_to_show[] = 'uid';
    }
    $entity = $this->entityTypeManager->getStorage('commerce_order')->create($order_values);

    $form = $this->formBuilder->getForm($entity, $form_mode);

    // Move Customer field out of any groups.
    unset($form['uid']['#groups']);

    // Hide any additional fields that have been rendered.
    foreach (Element::children($form) as $key) {
      if (!in_array($key, $elements_to_show)) {
        $form[$key]['#access'] = FALSE;
      }
    }

    return $form;
  }

}

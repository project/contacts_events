<?php

namespace Drupal\contacts_events\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for displaying a user's events (past and upcoming bookings).
 *
 * @package Drupal\contacts_events\Controller
 */
class UserEventsController extends ControllerBase {

  /**
   * Block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(BlockManagerInterface $block_manager, ContextHandlerInterface $context_handler) {
    $this->blockManager = $block_manager;
    $this->contextHandler = $context_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('context.handler')
    );
  }

  /**
   * Checks access to the given user's events page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user whose events are being viewed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(UserInterface $user, AccountInterface $account) {
    $result = AccessResult::allowedIfHasPermission($account, 'can manage bookings for contacts_events')
      ->orIf(AccessResult::allowedIf($account->id() == $user->id())
        ->addCacheableDependency($user)
        ->addCacheableDependency($account));
    if ($result instanceof AccessResultReasonInterface) {
      $result->setReason('User not allowed to manage bookings.');
    }
    return $result;
  }

  /**
   * Displays past and upcoming bookings.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user being viewed.
   *
   * @return array
   *   Render array.
   */
  public function events(UserInterface $user) {
    $build = [];

    // Convert the AccountInterface back into an underlying user entity.
    $current_user = $this->entityTypeManager()->getStorage('user')
      ->load($this->currentUser()->id());

    $contexts = [
      'current_user' => EntityContext::fromEntity($current_user),
      'user' => EntityContext::fromEntity($user),
    ];

    $build['upcoming_title'] = [
      '#markup' => $this->t('<h2>Upcoming events</h2>'),
    ];

    $build['upcoming'] = $this->buildBlock('views_block:contacts_events_events-upcoming', $contexts);

    $build['past_title'] = [
      '#markup' => $this->t('<h2>Past events</h2>'),
    ];

    $build['past'] = $this->buildBlock('views_block:contacts_events_events-past', $contexts);

    return $build;
  }

  /**
   * Builds the block output.
   *
   * @param string $block_id
   *   Block ID to build.
   * @param array $contexts
   *   Contexts for the block.
   *
   * @return array
   *   Render array.
   */
  private function buildBlock($block_id, array $contexts) {
    $block = $this->blockManager->createInstance($block_id);
    if ($block instanceof ContextAwarePluginInterface) {
      $this->contextHandler->applyContextMapping($block, $contexts);
    }
    return $block->build();
  }

}

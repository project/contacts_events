<?php

/**
 * @file
 * Post update functions for Contacts Events module.
 */

use Drupal\search_api\Item\Field;

/**
 * Adds Booking manager and delegate reference fields to the search index.
 */
function contacts_events_post_update_add_user_booking_search_fields(&$sandbox = NULL) {
  // Check whether Search API is enabled.
  $entity_type_manager = \Drupal::entityTypeManager();
  if (!$entity_type_manager->hasDefinition('search_api_index')) {
    return;
  }

  // Check our index exists.
  /** @var \Drupal\search_api\IndexInterface $index */
  $index = $entity_type_manager->getStorage('search_api_index')->load('contacts_index');
  if (!$index) {
    return;
  }

  // Create the Booking Manager booking reference field.
  $bm_field = new Field($index, 'bm_order_number');
  $bm_field->setType('text');
  $bm_field->setPropertyPath('ce_bookings_managed:entity:order_number');
  $bm_field->setDatasourceId('entity:user');
  $bm_field->setLabel('Bookings (manager) » Order » Order number');
  $bm_field->setBoost(8);
  if (!$index->getField($bm_field->getFieldIdentifier())) {
    $index->addField($bm_field);
  }

  // Create the Delegate booking reference field.
  $delegate_field = new Field($index, 'delegate_order_number');
  $delegate_field->setType('text');
  $delegate_field->setPropertyPath('ce_bookings_delegate:entity:order_number');
  $delegate_field->setDatasourceId('entity:user');
  $delegate_field->setLabel('Bookings (delegate) » Order » Order number');
  $delegate_field->setBoost(5);
  if (!$index->getField($delegate_field->getFieldIdentifier())) {
    $index->addField($delegate_field);
  }

  $index->save();
}

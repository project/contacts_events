/**
 * @file
 * Provides delayed events for AJAX handlers.
 */

(function ($, Drupal) {

  /**
   * Attaches the delayed events to each Ajax form element.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Register the delayed events for all relevant elements in
   *   `drupalSettings.ajax`.
   */
  Drupal.behaviors.DelayedEvents = {
    timeouts: {},
    attach: function attach(context, settings) {
      Object.keys(settings.ajax || {}).forEach(function (base) {
        var elementSettings = settings.ajax[base];
        if (elementSettings.event.substr(0, 8) === 'delayed.') {
          $(elementSettings.selector).once('delayed-events').each(function () {
            var $this = $(this);
            var id = elementSettings.delayed_group_id || $this.uniqueId().attr('id');
            var delay = elementSettings.delayed_time || 500;
            $this.on(elementSettings.event.substr(8), function () {
              clearTimeout(Drupal.behaviors.DelayedEvents.timeouts[id]);
              Drupal.behaviors.DelayedEvents.timeouts[id] = setTimeout(function () {
                Drupal.behaviors.DelayedEvents.timeouts[id] = null;
                $this.trigger(elementSettings.event);
              }, delay);
            });

            if (elementSettings.delayed_focus || true) {
              $this.on('focus', function () {
                if (Drupal.behaviors.DelayedEvents.timeouts[id]) {
                  clearTimeout(Drupal.behaviors.DelayedEvents.timeouts[id]);
                  Drupal.behaviors.DelayedEvents.timeouts[id] = true;
                }
              });
              $this.on('blur', function () {
                if (Drupal.behaviors.DelayedEvents.timeouts[id] === true) {
                  clearTimeout(Drupal.behaviors.DelayedEvents.timeouts[id]);
                  Drupal.behaviors.DelayedEvents.timeouts[id] = setTimeout(function () {
                    Drupal.behaviors.DelayedEvents.timeouts[id] = null;
                    $this.trigger(elementSettings.event);
                  }, delay);
                }
              });
            }
          });
        }
      });
    }
  };

  /**
   * Prepare the Ajax request before it is sent.
   *
   * Overrides core to add the no_disable option.
   *
   * @param {XMLHttpRequest} xmlhttprequest
   *   Native Ajax object.
   * @param {object} options
   *   jQuery.ajax options.
   */
  Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    if (this.$form) {
      options.extraData = options.extraData || {};

      options.extraData.ajax_iframe_upload = '1';

      var v = $.fieldValue(this.element);
      if (v !== null) {
        options.extraData[this.element.name] = v;
      }
    }

    if (!this.elementSettings.no_disable) {
      $(this.element).prop('disabled', true);
    }

    if (!this.progress || !this.progress.type) {
      return;
    }

    var progressIndicatorMethod = 'setProgressIndicator' + this.progress.type.slice(0, 1).toUpperCase() + this.progress.type.slice(1).toLowerCase();
    if (progressIndicatorMethod in this && typeof this[progressIndicatorMethod] === 'function') {
      this[progressIndicatorMethod].call(this);
    }
  };

})(jQuery, Drupal);

<?php

/**
 * @file
 * Views execution hook implementations for Contacts Events.
 */

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_alter().
 */
function contacts_events_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() === 'contacts_events_booking_balances') {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $table = $query->getTableInfo('commerce_order')['alias'];
    $operator = substr($view->current_display, 0, 8) === 'balances' ? '>' : '<';
    $query->addWhereExpression(0, "COALESCE({$table}.total_price__number, 0) {$operator} COALESCE({$table}.total_paid__number, 0)");
  }
}

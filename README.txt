INTRODUCTION
------------

  * This module extends the Contacts module to add events with ticketing and
    optional accommodation, camping accommodation and ticket printing. The
    module uses Drupal Commerce to manage user payment for tickets. All users
    booking on events or being registered for tickets are automatically added as
    Contacts on the site, allowing them to be tracked and managed.

  * The module also includes optional sub-modules to provide additional 
    functionality:

    - Contacts Events Accommodation

      Adds bookable accommodation to Events.

    - Commerce Advanced Queue

      Adds advanced queue support to Drupal Commerce queues.

    - Contacts Event Segments

      Adds Day Ticket functionality to Events, allowing users to purchase
      single day tickets for multi-day events.

    - Contacts Events Printing

      Adds PDF exports to Event Tickets.

    - Contacts Events Teams

      Adds support for volunteer teams to Events.

    - Contacts Events Villages

      Allows users to request plots in camping sites when booking an event. The
      functionality of the module is limited and it is unlikely it will be of
      much use presently.

    - Contacts Events Village Allocation

      This module is currently under development and is not suitable for use.

REQUIREMENTS
------------

  * This module requires the following modules:

    - Contacts (/project/contacts)
    - CRM User Dashboard (Sub-module of Contacts)
    - Commerce Advanced Queue (Sub-module of this module)
    - Commerce Core (/project/commerce)
    - Entity API (/project/entity)
    - Field Group (/project/name)
    - Views Data Export (/project/views_data_export)

  * There are also a number of sub-modules with their own dependencies:
    - Commerce Advanced Queue
       - Advanced Queue (/project/advancedqueue)
    - Contacts Events Villages
       - Contacts Events Accommodation (Sub-module of this module)
       - Contacts Events Teams (Sub-module of this module)

       This module also requires that a private file system is set up.

    - Contacts Events Printing
       - This module requires that wkhtmltopdf is installed and executable by
         the www-data user in /usr/local/bin/wkhtmltopdf on your website's
         server. The creation of PDFs will generate an error if this is not the
         case.

INSTALLATION
------------

  * Because we are currently relying on some core and contrib patches, you will
    need to either manually patch some of our dependencies or use composer with
    the composer patches project. It is recommended to use
    drupal-composer/drupal-project

  * As this module requires the Contacts module, patching should already have
    been set up for that. However, on a completely clean install you can install
    Contacts Events with the following commands:

        composer config extra.enable-patching true
        composer require drupal/contacts_events
        drush en contacts_events

 * Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Before Events can be booked by users, Commerce Core needs to be configured
   to manage the payment side. Instructions for this process are available at
   https://docs.drupalcommerce.org/, and require additional payment modules to
   be installed if you do not want to use Commerce Core's offline payment
   option. If Commerce Core is already set up on your site, this step can be
   skipped.

 * Once a Commerce Store and appropriate Payment Gateway(s) have been added, an
   administrator needs to visit /admin/config/contacts-events/booking-settings
   and select the appropriate store for Events to use.

 * Once completed, Events can be added by users with the Create new events
   permission at /event/add/{event type id}. If required, new event types can be
   added by users with the Administer events permission at
   /admin/structure/event_type.

 * Users with the View published events permission can view an event, and users
   with the Can book for events permission will be able to book a place at the
   event and pay for it in accordance with the Commerce Core settings.

 * A Child / Adult Ratio field is added to allow users adding Events to control
   the ratio of children to adults at the event. This field is hidden from
   display on the Event Type form display by default. To use this functionality,
   visit /admin/structure/event_type/{event type}/form-display and move the
   field into the visible area of the form. This will then allow users creating
   events to specify a ratio for their event.

 * Contacts Event Segments

   This module adds a Day Tickets field to events, which by default is hidden
   from display on the Event Type form display. To use this functionality, visit
   /admin/structure/event_type/{event type}/form-display and move the field into
   the visible area of the form. This will then allow users creating events to
   specify that Day Tickets can be purchased for their event.

   Once the settings has been enabled, the specific segments (days) will need to
   be set at /event/{event id}/segments/add by the user creating the event.

   In order for users to select their desired Day Tickets when booking, the
   Segments field will need to be moved into the visible area of the Event's
   Ticket Type entity at
   /admin/structure/contacts_ticket_type/{ticket type}/edit/form-display/booking

 * Contacts Events Printing

   This module adds an Enable Ticket Printing field to the Event Types. To use
   this functionality, the field must be moved into the visible area of your
   Event Type's form display at
   /admin/structure/event_type/{event type}/form-display. This will then
   display a checkbox in the form.

   If the checkbox is checked when adding/editing an Event, users with tickets
   linked to a completed order will see additional Print and Download E-Ticket
   links when they view their ticket. The Print link displays the ticket in the
   browser to allow users to print it, and the Download E-Ticket link converts
   the ticket into a PDF that downloads automatically.

   This module requires that wkhtmltopdf is installed and executable by the
   www-data user in /usr/local/bin/wkhtmltopdf on your website's server and
   should not be enabled if this is not the case.

 * Contacts Events Accommodation

   This module adds a Camping accommodation type by default: any additional
   types required can be added at /admin/structure/event-accommodation. A field
   is added to the Event Type to allow the user adding an Event to select the
   Accommodation Types that are valid for the event. If accommodation types are
   selected, an Accommodation tab becomes available on the Event, where the
   accommodation can be added. This is at the path
   /event/{event id}/accommodation.

   If accommodation has been added to the event, during the booking process the
   user will be asked to select their requirements.

 * Contacts Events Teams

   The module allows Team Application Forms to be added to the site at
   /admin/structure/teams/forms. Questions to be asked during the application
   can be added to the forms as fields, at
   /admin/structure/teams/forms/{form type}/fields

   Emails sent during the application process are
   configurable at /admin/config/system/team-emails. A Team Category vocabulary
   is created by the module, and MUST have categories added at
   /admin/structure/taxonomy/manage/contacts_events_teams/add before the teams
   can be correctly created.

   The Events Type has a Teams Settings field added, which will need to be moved
   into the visible area of the form display before the functionality can be
   used. Once this has been moved at
   /admin/structure/event_type/{event type}/form-display, the user adding or
   editing the Event can specify the type of team applications that can be made:
   published or private. This also allows a minimum age for team applicants to
   be set.

   Teams can then be added and managed at /event/{event}/teams/manage. If a
   Published team is used, then during the booking process the user will have
   the opportunity to apply as a volunteer to the teams. If a price override is
   added to the Team, the user will pay the overriden price for their ticket,
   allowing you to add a discount for volunteering. Their application can then
   be managed from the manage tab. If the Event has published teams set, but a
   private team is added, this will not be available for users without the
   can manage bookings for contacts_events permission to select when creating
   their tickets.

   If an Event is set to use private teams, only users with the can manage
   bookings for contacts_events permission will be able to select team
   applications when creating tickets. In these cases, users with the permission
   will need to add tickets for the users that need to be invited to join the
   team.

 * Contacts Events Villages

   The module creates a Village Requirements taxonomy vocabulary where the
   available facilities in a village can be added, with icons. The module adds a
   Village Group types field to the Event type, and gives options for selecting
   either no group, and Organisation group or an Other group. If no group is
   selected, Village Groups will be disabled for the event. If you select
   Organisation Group, this will link your Village Group to a selected
   Organisation added as a Contact to your site.

   If Village Groups have been selected for an event, Villages can be added and
   managed from /event/{event id}/accommodation/villages. Village Groups can be
   added and managed from /event/{event id}/accommodation/village-groups,
   depending on the types selected on the Event itself.

   When Villages and Village Groups have been added to the event, users will
   see an option to select which Village Group they would like to stay in on
   the Accommodation section of the booking form for the event. If users select
   that they want to stay in an Organisation Village Group, they will be
   required to enter the name of te organisation before they can join the group.

   At this point, the module provides no further function. At present, there is
   nothing in the module to move the applications into the best suited
   villages. This would be done by an allocation process, which the Contacts
   Events Village Allocation is being developed to provide.

 * Contacts Events Village Allocation

   This module is not currently ready for use, and should not be enabled.

   The module creates a database search index using the database search server
   created by the Contacts module, and provides facets to allow the users to be
   filtered. If you wish to use another backend for your search index, you will
   need to replicate the view and its related Facets in your new backend.

   If you use the Contacts Theme, the facets for the search index will
   automatically display in the left-hand column of the page. If you are not
   using the Contacts Theme, these will need to be placed appropriately in
   your preferred admin theme (if required). Additional facets based on your
   contacts' information can be added as usual via the facets user interface.

TROUBLESHOOTING
---------------

 * If you visit /admin/config/contacts-events/booking-settings and get an error
   about the Store field being required preventing you from saving the form, you
   will need to set up a Commerce Store before you can complete the form.

 * If your users receive an error about Payment Gateways when trying to book
   onto an event, you will need to set up a working Payment Gateway in
   Commerce Core.

 * If you have enabled the Contacts Event Segments module but cannot specify
   that your Event Type should allow day tickets, edit the form display of your
   event type to display the Day Tickets field in the form.

 * If you have enabled the Day Tickets field in the Event Type form display, but
   users cannot select Day Tickets when they are booking, make sure you have
   added segments to the Event's using its segment tab.

 * If you have added Contacts Event Segments to an Event, but users cannot
   select Day Tickets when they are booking, check that you have added the
   segments field in the visible display of the Ticket Type used by the Event.

 * If clicking on the Download E-Ticket link on your ticket generates an error,
   check that wkhtmltopdf is installed and executable by the www-data user
   in /usr/local/bin/wkhtmltopdf on your website's server.

 * If you get a notice that the private file system has not been set up when
   trying to upload Village Group files, you will need to set up your private
   file system.

 * If you are trying to add an Organisation Village Group at
   /event/{event}/accommodation/village-groups/add/organisation but cannot get
   any results in the required Organisation field, ensure that you have added
   at least one Organisation Contact to your site, and that your Contacts
   search api index is up-to-date and indexed.

FAQ
---

 Q: I don't like the way my tickets print: how can I change them?
 A: The tickets use a custom template for rendering, so this can be overridden
    in your own theme and a HOOK_preprocess_TEMPLATE implementation.

MAINTAINERS
-----------

Current maintainers:
 * Andrew Belcher (andrewbelcher) - /u/andrewbelcher
 * Yan Loetzer (yanniboi) - /u/yanniboi
 * Paul Smith (MrDaleSmith) - /u/mrdalesmith

This project has been sponsored by:
 * Freely Give
   We’re FreelyGive, Drupal specialists developing Full Stack Customer
   Relationship Management (CRM) solutions your users will love.
   Visit https://freelygive.io/ for more information.

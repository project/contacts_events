<?php

namespace Drupal\contacts_events_teams;

use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\contacts_events_teams\Entity\Team;
use Drupal\contacts_events_teams\Entity\TeamInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Utility\Token;

/**
 * Sends team emails.
 *
 * @package Drupal\contacts_events_teams
 */
class TeamEmailService {

  /**
   * The teams email config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The mail plugin manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * TeamEmailSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Utility\Token $token
   *   Token replacement service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Token $token, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager) {
    $this->config = $config_factory->get('contacts_events_teams.emails');
    $this->token = $token;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Sends an email to the team applicant.
   *
   * @param string $email_key
   *   ID of the email.
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket.
   * @param \Drupal\contacts_events_teams\Entity\Team $team
   *   The team.
   */
  public function sendToApplicant($email_key, TicketInterface $ticket, Team $team) {
    // Check if emails are disabled for this team.
    if ($team->get('disable_automatic_emails')->value) {
      return;
    }

    // If we are sending the application request and we already have a team,
    // see if the selected team email is enabled.
    if ($email_key == 'application_request' && $team && $this->applicationTeamEmailEnabled($team)) {
      $email_key = 'application_request_team';
    }

    // Get the email config from the team if overridden or from the defaults.
    // Emails with key 'application_request' cannot have a team specific email.
    if ($email_key != 'application_request' && $this->teamSpecificOverride($team)) {
      $email_config = [
        'enabled' => $team->get($email_key . '_enabled')->value,
        'skip_booking_manager' => FALSE,
        'subject' => $team->get($email_key . '_subject')->value,
        'body' => [
          'value' => $team->get($email_key . '_body')->value,
          'format' => $team->get($email_key . '_body')->format,
        ],
      ];

      // Not every email key has a booking manager field so check if it exists
      // before checking the value.
      if ($team->hasField($email_key . '_b_man')) {
        $email_config['skip_booking_manager'] = $team->get($email_key . '_b_man')->value;
      }
    }
    else {
      $email_config = $this->config->get("emails.{$email_key}");
    }

    $order_item = $ticket->getOrderItem();

    // If we haven't got an enabled email, return now.
    if (empty($email_config['enabled'])) {
      return;
    }

    // If the ticket holder is the booking manager, we may want to skip sending
    // to the booking manager.
    if (!empty($email_config['skip_booking_manager'])) {
      $order = $order_item->getOrder();
      if ($order->getCustomerId() == $ticket->get('contact')->target_id) {
        return;
      }
    }

    // Build our token data.
    $token_data = [
      'contacts_ticket' => $ticket,
      'c_events_team' => $team,
      'contacts_event' => $ticket->getEvent(),
    ];

    // Get our parts of the email.
    $to = "{$ticket->getName('plain')} <{$ticket->get('email')->value}>";
    $params['subject'] = $this->token->replace($email_config['subject'], $token_data);
    $params['body'] = $this->token->replace($email_config['body']['value'], $token_data);
    $params['body_format'] = $this->token->replace($email_config['body']['format'], $token_data);
    $params['from'] = $this->config->get('from');

    /** @var \Drupal\user\UserInterface $user */
    $user = $ticket->get('contact')->entity;
    if ($user) {
      $langcode = $user->getPreferredLangcode();
    }
    else {
      $langcode = $this->languageManager->getDefaultLanguage()->getId();
    }

    $this->mailManager->mail('contacts_events_teams', $email_key, $to, $langcode, $params);
  }

  /**
   * Check whether a Team has email overrides enabled.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamInterface $team
   *   The team to check for overrides.
   *
   * @return bool
   *   TRUE if the team has email overrides enabled. FALSE otherwise.
   */
  protected function teamSpecificOverride(TeamInterface $team): bool {
    return (bool) $team->get('team_email_override')->value;
  }

  /**
   * Check if the team specific application email is enabled for this team.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamInterface $team
   *   The team to check the settings for.
   *
   * @return bool
   *   TRUE if the team email is enabled, FALSE otherwise.
   */
  protected function applicationTeamEmailEnabled(TeamInterface $team): bool {
    if ($this->teamSpecificOverride($team)) {
      return (bool) $team->get('application_request_team_enabled')->value;
    }

    // Otherwise use the default setting.
    return (bool) $this->config->get('emails.application_request_team.enabled');
  }

}

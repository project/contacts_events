<?php

namespace Drupal\contacts_events_teams\EventSubscriber;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Event\CloneEvent;
use Drupal\contacts_events\EventSubscriber\CloneEventDependentEntitySubscriberBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Teams clone event subscriber.
 */
class TeamsCloneEventSubscriber extends CloneEventDependentEntitySubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function onClone(CloneEvent $event): void {
    $event->addOperation(
      'contacts_events_teams.clone_event_subscriber',
      'cloneTeamsOperation',
      new TranslatableMarkup('Cloning teams'),
    );
  }

  /**
   * Clone operation to clone teams.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The new event.
   * @param \Drupal\contacts_events\Entity\EventInterface $source
   *   The source event.
   * @param array $sandbox
   *   A sandbox for storing data that will be preserved between calls to this
   *   operation.
   *
   * @return int
   *   The progress percentage between 0 (not started) and 100 (complete).
   */
  public function cloneTeamsOperation(EventInterface $event, EventInterface $source, array &$sandbox): int {
    return $this->cloneDependantEntityOperation('c_events_team', $event, $source, $sandbox, 'event');
  }

}

<?php

namespace Drupal\contacts_events_teams\EventSubscriber;

use Drupal\contacts_dbs\DBSManager;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for handling team application state transitions.
 *
 * This event subscriber depends on the contacts_dbs module.
 *
 * @package Drupal\contacts_events\EventSubscriber
 */
class DbsTeamApplicationStateSubscriber implements EventSubscriberInterface {

  /**
   * The DBS manager service.
   *
   * @var \Drupal\contacts_dbs\DBSManager
   */
  protected $dbsManager;

  /**
   * Construct the team application state subscriber event subscriber.
   *
   * @param \Drupal\contacts_dbs\DBSManager $dbs_manager
   *   The price calculator.
   */
  public function __construct(DBSManager $dbs_manager) {
    $this->dbsManager = $dbs_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Start a DBS check when an application is submitted.
    $events['contacts_events_teams_applications.submit.post_transition'][] = ['startDbsCheck'];

    return $events;
  }

  /**
   * Triggered after the team application is moved to the submitted state.
   *
   * If the team being applied for requires DBS this will start a check for the
   * relevant workforce.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function startDbsCheck(WorkflowTransitionEvent $event) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $team_app */
    $team_app = $event->getEntity();
    $team = $team_app->getTeam();
    if (!$team || $team->get('dbs_workforce')->isEmpty()) {
      return;
    }

    $workforce = $team->get('dbs_workforce')->value;

    $event = $team->getEvent();
    $valid_at = $event->get('date')->start_date;

    $message = "Team application {$team_app->id()} for {$event->label()} submitted.";
    $this->dbsManager->start($team_app->getOwnerId(), $workforce, $valid_at, $message);
  }

}

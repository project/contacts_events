<?php

namespace Drupal\contacts_events_teams\Entity;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Team entities.
 *
 * @ingroup contacts_events_teams
 */
interface TeamInterface extends ContentEntityInterface {

  /**
   * Event team applications are closed.
   */
  const STATUS_CLOSED = 0;

  /**
   * Event team applications are open.
   */
  const STATUS_OPEN = 1;

  /**
   * Event team applications are private.
   */
  const STATUS_PRIVATE = 2;

  /**
   * Gets the Team name.
   *
   * @return string
   *   Name of the Team.
   */
  public function getName();

  /**
   * Sets the Team name.
   *
   * @param string $name
   *   The Team name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the event associated with this team.
   *
   * @return \Drupal\contacts_events\Entity\EventInterface
   *   The event.
   */
  public function getEvent() : EventInterface;

  /**
   * Sets the event for this team.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface|int $event
   *   Event or event ID.
   *
   * @return $this
   */
  public function setEvent($event);

  /**
   * Checks whether the specified user is a team leader for this team.
   *
   * @param \Drupal\Core\Session\AccountInterface|int $user
   *   The User or User ID.
   *
   * @return bool
   *   Whether the user is a team leader.
   */
  public function isTeamLeader($user) : bool;

}

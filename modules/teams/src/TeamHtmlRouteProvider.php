<?php

namespace Drupal\contacts_events_teams;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Team entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class TeamHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getCollectionRoute($entity_type)) {
      $route->setOption('_admin_route', TRUE);
      $route->setOption('parameters', [
        'contacts_event' => ['type' => 'entity:contacts_event'],
      ]);
      $route->setRequirement('_permission', 'manage contacts events teams');
      $route->setRequirement('_contacts_events_teams', 'TRUE');
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('canonical')) {
      $team_app = $this->entityTypeManager->getDefinition('c_events_team_app');
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $label */
      $label = $team_app->getCollectionLabel();

      $route = new Route($entity_type->getLinkTemplate('canonical'));
      $route
        ->addDefaults([
          '_entity_list' => $team_app->id(),
          '_title' => $label->getUntranslatedString(),
          '_title_arguments' => $label->getArguments(),
          '_title_context' => $label->getOption('context'),
        ])
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          'contacts_event' => ['type' => 'entity:contacts_event'],
          'c_events_team' => ['type' => 'entity:c_events_team'],
        ])
        ->setRequirement('_entity_access', 'c_events_team.view');

      return $route;
    }
  }

}

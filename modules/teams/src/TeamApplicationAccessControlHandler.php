<?php

namespace Drupal\contacts_events_teams;

use Drupal\contacts_events_teams\Entity\TeamInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Team application entity.
 *
 * @see \Drupal\contacts_events_teams\Entity\TeamApplication.
 */
class TeamApplicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_events\Entity\EventInterface $event */
    $event = $entity->get('event')->entity;

    if (!$event) {
      return AccessResult::forbidden("This team application doesn't have an event.");
    }

    $event_team_status = (int) $event->getSetting('teams.enabled', TeamInterface::STATUS_CLOSED);

    if ($event_team_status === TeamInterface::STATUS_CLOSED) {
      return AccessResult::forbidden('Teams are not public for this event.')
        ->addCacheableDependency($event);
    }

    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $entity */
    switch ($operation) {
      // Defer to the team for who can view and manage applications.
      case 'view':
      case 'manage':
        if ($entity->getTeam()) {
          return $entity->getTeam()->access('view', $account, TRUE);
        }
        // If the Team is missing (i.e. it's been deleted since the application
        // was created) then only allow staff to view it.
        return AccessResult::allowedIfHasPermission($account, 'manage all contacts events team applications');

      // Update only available to staff and the person who made the application,
      // so long as the application isn't submitted yet and applications are
      // still open.
      case 'update':
        // Update only available to staff and the person who made the
        // application, so long as the application isn't submitted yet.
        // Staff can update if teams are open/private, applicants can only
        // update if open.
        return AccessResult::allowedIfHasPermission($account, 'manage all contacts events team applications')
          ->orIf(AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $entity->get('state')->value === 'draft' && $event_team_status === TeamInterface::STATUS_OPEN)
            ->addCacheableDependency($entity->getOwner()));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add team applications');
  }

}

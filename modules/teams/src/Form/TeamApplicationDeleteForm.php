<?php

namespace Drupal\contacts_events_teams\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Team application entities.
 *
 * @ingroup contacts_events_teams
 */
class TeamApplicationDeleteForm extends ContentEntityDeleteForm {


}

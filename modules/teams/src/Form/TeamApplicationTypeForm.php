<?php

namespace Drupal\contacts_events_teams\Form;

use Drupal\contacts_events_teams\Entity\TeamApplicationType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Allows for adding/editing team application forms.
 */
class TeamApplicationTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\contacts_events_teams\Entity\TeamApplicationType $team_form */
    $team_form = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $team_form->label(),
      '#description' => $this->t("Label for the Team application type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $team_form->id(),
      '#machine_name' => [
        'exists' => [TeamApplicationType::class, 'load'],
      ],
      '#disabled' => !$team_form->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#description' => $this->t('Whether the application form is available for selection on new teams.'),
      '#default_value' => $team_form->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplicationType $team_form */
    $team_form = $this->entity;
    $status = $team_form->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label form.', [
          '%label' => $team_form->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label form.', [
          '%label' => $team_form->label(),
        ]));
    }
    $form_state->setRedirectUrl($team_form->toUrl('collection'));
  }

}

<?php

namespace Drupal\contacts_events_teams\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Contacts Events Teams settings for this site.
 */
class TeamEmailsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Construct the teams email form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_events_teams_team_emails';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['contacts_events_teams.emails'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contacts_events_teams.emails');
    $site_config = $this->config('system.site');

    $form['tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#group' => 'tabs',
      '#title' => $this->t('General settings'),
      '#weight' => -99,
    ];
    $form['settings']['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email from address'),
      '#description' => $this->t('The name and email the team emails should come from. E.g. %example. Defaults to the site wide settings.', [
        '%example' => 'Example teams <teams@example.com>',
      ]),
      '#placeholder' => "{$site_config->get('name')} <{$site_config->get('mail')}>",
      '#default_value' => $config->get('from'),
    ];

    $this->addTeamEmailConfig('application_request',
      $this->t('Application request (no team)'),
      $form,
      $form_state,
      $this->t('This email is sent when a team ticket is confirmed by the booking manager requesting the team applicant fill out their application.'));
    $form['application_request']['skip_booking_manager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not send to the booking manager.'),
      '#description' => $this->t('Do not send this email if the ticket holder is the booking manager.'),
      '#default_value' => $config->get('emails.application_request.skip_booking_manager'),
      '#weight' => -90,
      '#states' => $this->getEnableStates('application_request'),
    ];

    $this->addTeamEmailConfig('application_request_team',
      $this->t('Application request (with team)'),
      $form,
      $form_state,
      $this->t('This email is sent when a team ticket is confirmed by the booking manager with a specified team requesting the team applicant fill out their application. If not enabled, it will fall back to the %default email, if enabled.', [
        '%default' => $this->t('Application request (no team)'),
      ]));
    $form['application_request_team']['skip_booking_manager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not send to the booking manager.'),
      '#description' => $this->t('Do not send this email if the ticket holder is the booking manager.'),
      '#default_value' => $config->get('emails.application_request_team.skip_booking_manager'),
      '#weight' => -90,
      '#states' => $this->getEnableStates('application_request_team'),
    ];

    $this->addTeamEmailConfig('application_accepted',
      $this->t('Application Accepted'),
      $form,
      $form_state,
      $this->t('This email is sent when a team ticket is accepted.'));

    $this->addTeamEmailConfig('application_submitted',
      $this->t('Application Submitted'),
      $form,
      $form_state,
      $this->t('This email is sent to the applicant after submission. It is not sent when staff members manually mark an application and submitted on the back-end.'));

    return parent::buildForm($form, $form_state);
  }

  /**
   * Add an email configuration tab to the form.
   *
   * @param string $key
   *   The mail key.
   * @param mixed $title
   *   The title for the email.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param mixed $description
   *   The description, if any, for the email.
   */
  public function addTeamEmailConfig(string $key, $title, array &$form, FormStateInterface $form_state, $description = NULL) {
    $form[$key] = [
      '#type' => 'details',
      '#group' => 'tabs',
      '#title' => $title,
      '#description' => $description,
      '#parents' => ['emails', $key],
      '#tree' => TRUE,
    ];

    $config = $this->config('contacts_events_teams.emails')->get("emails.{$key}");

    $form[$key]['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable %title', ['%title' => $title]),
      '#default_value' => $config['enabled'] ?? FALSE,
      '#weight' => -99,
    ];

    $form[$key]['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('The subject for the email.'),
      '#default_value' => $config['subject'] ?? '',
      '#states' => $this->getEnableStates($key, TRUE),
    ];

    $form[$key]['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => $config['body']['value'] ?? '',
      '#states' => $this->getEnableStates($key, TRUE),
      '#rows' => 20,
    ];
    if (isset($config['body']['format'])) {
      $form[$key]['body']['#format'] = $config['body']['format'];
    }

    if ($this->moduleHandler->moduleExists('token')) {
      $form[$key]['tokens'] = [
        '#type' => 'container',
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'site',
          'contacts_ticket',
          'c_events_team',
          'contacts_event',
        ],
        '#global_types' => FALSE,
        '#states' => $this->getEnableStates($key),
      ];
    }
  }

  /**
   * Build the states array for fields dependent on an enabled email.
   *
   * @param string $key
   *   The email key.
   * @param bool $required
   *   Whether the field should be requires (does not perform validation).
   *
   * @return array
   *   The array for the '#states' key.
   */
  public function getEnableStates(string $key, bool $required = FALSE) : array {
    $states = [
      'visible' => [':input[name="emails[' . $key . '][enabled]"]' => ['checked' => TRUE]],
    ];
    if ($required) {
      $states['required'] = [':input[name="emails[' . $key . '][enabled]"]' => ['checked' => TRUE]];
    }
    return $states;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('emails') as $key => $values) {
      // Don't do additional validation for disabled emails.
      if (!$values['enabled']) {
        continue;
      }

      if (empty($values['subject'])) {
        $form_state->setError($form[$key]['subject'], $this->t('%title subject is required.', [
          '%title' => $form[$key]['#title'],
        ]));
      }
      if (empty($values['body'])) {
        $form_state->setError($form[$key]['subject'], $this->t('%title subject is required.', [
          '%title' => $form[$key]['#title'],
        ]));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('contacts_events_teams.emails')
      ->set('emails', $form_state->getValue('emails'))
      ->set('from', $form_state->getValue('from'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\contacts_events_teams\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Form controller for Team edit forms.
 *
 * @ingroup contacts_events_teams
 */
class TeamForm extends ContentEntityForm {

  /**
   * The team entity.
   *
   * @var \Drupal\contacts_events_teams\Entity\TeamInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    /** @var \Drupal\contacts_events_teams\Entity\Team $team */
    $team = parent::getEntityFromRouteMatch($route_match, $entity_type_id);

    // Pre-populate event ID from URL.
    if ($team->isNew()) {
      $team->setEvent($route_match->getParameter('contacts_event'));
    }

    return $team;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Make event read-only.
    $event_field = $this->entity->get('event');
    $form['event'] = $event_field->view();

    // Filter the list of forms to only those that are active (plus current).
    $this->filterApplicationForms($form['form']['widget']['#options']);

    // Adjust wording of team leaders 'Add another item' button.
    $form['leaders']['widget']['add_more']['#value'] = $this->t('Add another team leader');

    // Add the event minimum value for the min age description.
    if ($min_age = $event_field->entity->getSetting('teams.min_age')) {
      $form['age_min']['widget'][0]['value']['#description'] = $this->t('@original The event minimum of %min will always apply.', [
        '@original' => $form['age_min']['widget'][0]['value']['#description'],
        '%min' => $this->formatPlural($min_age, '1 year', '@count years'),
      ]);
    }

    // Rearrange the team email fields.
    $this->overrideTeamEmailTabs($form);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label team for %event.', [
            '%label' => $entity->label(),
            '%event' => $entity->getEvent()->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label team for %event.', [
            '%label' => $entity->label(),
            '%event' => $entity->getEvent()->label(),
          ]));
    }
    $form_state->setRedirect('entity.c_events_team.collection', [
      'contacts_event' => $entity->getEvent()->id(),
    ]);
  }

  /**
   * Filters the list of enabled application forms.
   *
   * @param array $options
   *   The team form options.
   */
  private function filterApplicationForms(array &$options) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplicationType[] $app_forms */
    $app_forms = $this->entityTypeManager
      ->getStorage('c_events_team_app_type')
      ->loadMultiple(array_keys($options));

    // Only include enabled forms plus the one that's currently set on the
    // team (if any).
    foreach ($options as $app_form_id => $label) {
      if ($app_form_id !== '_none' && $app_form_id !== $this->entity->form->target_id) {
        if (!$app_forms[$app_form_id]->status()) {
          unset($options[$app_form_id]);
        }
      }
    }
  }

  /**
   * Helper function to do all of the team email customisations.
   *
   * @param array $form
   *   The current form array.
   */
  protected function overrideTeamEmailTabs(array &$form) {
    $emails = [
      'application_request_team',
      'application_accepted',
      'application_submitted',
    ];

    // "Override team emails" option should be hidden if "disable team emails"
    // is checked.
    $form['team_email_override']['#states']['visible'] = [
      ':input[name="disable_automatic_emails[value]"]' => ['checked' => FALSE],
    ];

    foreach ($emails as $email_id) {
      // If the top-level "Override Team Emails" is unchecked, then hide the
      // specific overrides. Also hide them if "Disable automatic emails for
      // this team" is checked.
      $form[$email_id . '_enabled']['#states']['visible'] = [
        ':input[name="team_email_override[value]"]' => ['checked' => TRUE],
        // phpcs:ignore Squiz.Arrays.ArrayDeclaration.NoKeySpecified
        'and',
        ':input[name="disable_automatic_emails[value]"]' => ['checked' => FALSE],
      ];

      // For each of the override emails, ensure their body/subject are hidden
      // if the specific override is disabled.
      if (isset($form[$email_id . '_subject'])) {
        $form[$email_id . '_subject']['#states'] = $this->getEnableStates($email_id);
      }
      if (isset($form[$email_id . '_body'])) {
        $form[$email_id . '_body']['#states'] = $this->getEnableStates($email_id);

        // Add token tree.
        if ($this->moduleHandler->moduleExists('token')) {
          $form[$email_id . '_body']['tokens'] = [
            '#type' => 'container',
            '#theme' => 'token_tree_link',
            '#weight' => 20,
            '#token_types' => [
              'site',
              'contacts_ticket',
              'c_events_team',
              'contacts_event',
            ],
            '#global_types' => FALSE,
            '#states' => $this->getEnableStates($email_id),
          ];
        }

      }
      if (isset($form[$email_id . '_b_man'])) {
        $form[$email_id . '_b_man']['#states'] = $this->getEnableStates($email_id);
      }
    }
  }

  /**
   * Helper function to get the states for a team email enabled field.
   *
   * @param string $email_id
   *   The ID of the email.
   *
   * @return array
   *   A states array for the enabled value.
   */
  protected function getEnableStates(string $email_id): array {
    return [
      'invisible' => [
        [
          [':input[name="' . $email_id . '_enabled[value]"]' => ['checked' => FALSE]],
          'and',
          [':input[name="team_email_override[value]"]' => ['checked' => FALSE]],
        ],
        'or',
        [
          [':input[name="disable_automatic_emails[value]"]' => ['checked' => TRUE]],
        ],
      ],
    ];
  }

}

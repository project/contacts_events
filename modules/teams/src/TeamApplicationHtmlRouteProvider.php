<?php

namespace Drupal\contacts_events_teams;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Team application entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class TeamApplicationHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getCollectionRoute($entity_type)) {
      $route->setOption('_admin_route', TRUE);
      $params = $route->getOption('parameters');
      $params['contacts_event'] = ['type' => 'entity:contacts_event'];
      $route->setOption('parameters', $params);
      $route->setRequirement('_permission', 'manage all contacts events team applications');
      return $route;
    }
  }

}

<?php

namespace Drupal\contacts_events_teams\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Team application step.
 *
 * @see \Drupal\contacts_events_teams\Plugin\TeamApplicationStep\TeamApplicationStepManager
 * @see plugin_api
 *
 * @Annotation
 */
class TeamApplicationStep extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label for end users.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * Weight. Steps with lower weights run first.
   *
   * @var float
   */
  public $weight;

  // @codingStandardsIgnoreStart
  /**
   * Optional: The entity form display to use when building the form.
   *
   * @var string
   */
  public $form_display;
  // @codingStandardsIgnoreEnd

}

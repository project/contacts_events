<?php

namespace Drupal\contacts_events_teams\Access;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events_teams\Entity\TeamInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks if teams are enabled for the event.
 */
class TeamsAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event we are checking teams access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(EventInterface $contacts_event) {
    $result = AccessResult::allowedIf($contacts_event->getSetting('teams.enabled', TeamInterface::STATUS_CLOSED) != TeamInterface::STATUS_CLOSED)
      ->addCacheableDependency($contacts_event);
    if ($result instanceof AccessResultReasonInterface) {
      $result->setReason('Teams are not enabled for this event.');
    }
    return $result;
  }

}

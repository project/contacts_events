<?php

namespace Drupal\contacts_events_teams\Plugin\views\field;

use Drupal\contacts_events_teams\Entity\TeamApplication;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that works out the DBS Status for a team app.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ce_teams_dbs_status")
 */
class DbsStatus extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $app */
    $app = $this->getEntity($values);
    if ($app && $app->getTeam() && $app->getTeam()->hasField('dbs_workforce') && $workforce = $app->getTeam()->get('dbs_workforce')->value) {
      return $this->getDbsStatus($app, $workforce);
    }
  }

  /**
   * Check DBS record for specific status information.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamApplication $app
   *   The team application.
   * @param string $workforce
   *   The workforce to check against.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The public status label for DBS.
   *
   * @todo Inject this somehow.
   *
   * @see \Drupal\contacts_events_teams\Controller\TeamApplicationController::getDbsStatus
   */
  private function getDbsStatus(TeamApplication $app, $workforce): TranslatableMarkup {
    /** @var \Drupal\contacts_dbs\DBSManager $dbs_manager */
    $dbs_manager = \Drupal::service('contacts_dbs.dbs_manager');
    $dbs_record = $dbs_manager->getDbs($app->getOwnerId(), $workforce);
    $dbs_status = $dbs_record ? $dbs_record->get('status')->value : NULL;

    switch ($dbs_status) {
      case 'letter_required':
      case 'letter_sent':
      case 'dbs_expired':
        return $this->t('DBS required');

      case 'disclosure_requested':
      case 'update_service_check_required':
      case 'disclosure_review':
        return $this->t('DBS in progress');

      case 'dbs_clear':
      case 'update_service_checked':
      case 'dbs_exception':
      case 'disclosure_accepted':
      case 'living_abroad':
        return $this->t('DBS clear');

      case 'dbs_not_clear':
        return $this->t('DBS not clear');

      default:
        return $this->t('DBS Error');
    }
  }

}

<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\Component\Utility\SortArray;
use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\contacts_events_teams\Annotation\TeamApplicationStep;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Plugin manager for team application steps.
 */
class TeamApplicationStepManager extends DefaultPluginManager {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, FormBuilderInterface $form_builder, ClassResolverInterface $class_resolver, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    parent::__construct('Plugin/TeamApplicationStep', $namespaces, $module_handler, TeamApplicationStepInterface::class, TeamApplicationStep::class);
    $this->alterInfo('contacts_events_teams_application_steps');
    $this->setCacheBackend($cache_backend, 'drs_submission_fraud_check_plugins');
    $this->formBuilder = $form_builder;
    $this->classResolver = $class_resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    // Ensure that the cached list of definitions is always sorted by weight.
    uasort($definitions, [SortArray::class, 'sortByWeightElement']);
    return $definitions;
  }

  /**
   * Get a progress indicator for the current step.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket being applied for.
   * @param string|null $step_id
   *   The current step ID.
   *
   * @return array|null
   *   A renderable array for an item_list of steps. The current step will have
   *   the 'active' class as a wrapper attribute.
   */
  public function getProgressIndicator(TicketInterface $ticket, ?string $step_id) {
    $steps = $this->getDefinitions();
    $step_id = $step_id ?: array_keys($steps)[0];

    $items = [];
    foreach ($steps as $step) {
      $options = $step_id == $step['id'] ? ['class' => 'active'] : [];
      $items[] = [
        '#markup' => $step['label'],
        '#wrapper_attributes' => $options,
      ];
    }

    if ($items) {
      return [
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => ['class' => 'team-application-progress'],
        '#attached' => [
          'library' => ['contacts_events_teams/progress_indicator'],
        ],
      ];
    }

    return NULL;
  }

  /**
   * Gets the step form.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket we are applying for.
   * @param string|null $step_id
   *   Step ID to load. If the step ID is NULL, the first step is returned.
   *
   * @return array|null
   *   Form array, or null if step not found.
   */
  public function getStepForm(TicketInterface $ticket, ?string $step_id) {
    // Get hold of our step configuration.
    $definitions = $this->getDefinitions();

    if ($step_id === NULL) {
      $step = reset($definitions);
    }
    elseif (isset($definitions[$step_id])) {
      $step = $definitions[$step_id];
    }
    else {
      return NULL;
    }

    // Retrieve our form object.
    $form_object = $this->classResolver->getInstanceFromDefinition($step['class']);

    // Content entity forms need a few extra services.
    if ($form_object instanceof ContentEntityForm) {
      $form_object
        ->setStringTranslation($this->stringTranslation)
        ->setModuleHandler($this->moduleHandler)
        ->setEntityTypeManager($this->entityTypeManager);
    }

    // Set the ticket and configuration if required.
    if ($form_object instanceof TeamApplicationStepInterface) {
      $form_object
        ->setTicket($ticket)
        ->setConfiguration($step);
    }

    // Return our form.
    return $this->formBuilder->getForm($form_object);
  }

}

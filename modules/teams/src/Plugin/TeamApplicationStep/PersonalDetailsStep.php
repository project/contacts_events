<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Personal details step.
 *
 * @TeamApplicationStep(
 *   id = "personal",
 *   label = @Translation("A bit about you"),
 *   weight = 1,
 *   form_display = "team_application"
 * )
 */
class PersonalDetailsStep extends ContentEntityForm implements TeamApplicationStepInterface {

  use TeamStepsTrait {
    setTicket as traitSetTicket;
  }

  /**
   * {@inheritdoc}
   */
  public function setTicket(TicketInterface $ticket) {
    $this->traitSetTicket($ticket);
    $this->setEntity($ticket->getTicketHolder()->profile_crm_indiv->entity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function showRevisionUi() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // If the current default address is empty and there is a logged in user
    // then try and load a default address.
    $form_address_is_empty = !array_filter($form['crm_address']['widget'][0]['address']['#default_value']);
    $user_is_authenticated = $this->currentUser()->isAuthenticated();
    $user_is_ticket_holder = $this->currentUser()->id() == $this->getTicket()->getTicketHolderId();
    if ($form_address_is_empty && $user_is_authenticated && $user_is_ticket_holder) {
      $ticket_holder = $this->getTicket()->getTicketHolder();
      // If the ticket holder has a default customer profile then use the
      // address from that as the default.
      $profile = $this->entityTypeManager->getStorage('profile')->loadDefaultByUser($ticket_holder, 'customer');
      if ($profile && $profile->hasField('address') && !$profile->get('address')->isEmpty()) {
        $form['crm_address']['widget'][0]['address']['#default_value'] = $profile->get('address')->first()->getValue();
      }
    }

    // Make everything required.
    foreach ($form['crm_name']['widget'][0]['#components'] as &$component) {
      $component['#required'] = TRUE;
    }

    $form['crm_address']['widget'][0]['address']['#required'] = TRUE;
    $form['crm_dob']['widget'][0]['value']['#required'] = TRUE;
    $form['crm_phone']['widget'][0]['value']['#required'] = TRUE;
    $form['crm_gender']['widget']['#required'] = TRUE;
    $form['crm_photo']['widget'][0]['#required'] = TRUE;
    $form['crm_photo']['widget'][0]['#title'] = $this->t('Your Photo');
    $form['crm_photo']['widget'][0]['#description'] = $this->t('Please upload a head and shoulders photo of yourself for your team ID badge.');
    return $form;
  }

}

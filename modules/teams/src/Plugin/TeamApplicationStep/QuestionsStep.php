<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Personal details step.
 *
 * @TeamApplicationStep(
 *   id = "additional",
 *   label = @Translation("Additional info"),
 *   weight = 3,
 *   form_display = "default"
 * )
 */
class QuestionsStep extends ContentEntityForm implements TeamApplicationStepInterface {

  use TeamStepsTrait {
    setTicket as traitSetTicket;
  }

  /**
   * The team application.
   *
   * @var \Drupal\contacts_events_teams\Entity\TeamApplication
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return parent::create($container)
      ->setQueries($container->get('contacts_events_teams.queries'));
  }

  /**
   * {@inheritdoc}
   */
  public function setTicket(TicketInterface $ticket) {
    $this->traitSetTicket($ticket);
    $this->setEntity($this->getTeamApplication(TRUE));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate that reference email is not the same as the ticket holders.
    // @todo Move this to entity level validation.
    if ($this->moduleHandler->moduleExists('contacts_references')) {
      // Find any references and update their application & uid fields.
      /** @var \Drupal\contacts_references\Entity\Reference[] $reference_fields */
      $reference_fields = \Drupal::service('contacts_references.reference_field_helper')
        ->findReferences($this->entity);

      // Cannot access the actual reference entities as they are created on
      // submission.
      foreach (array_keys($reference_fields) as $field_name) {
        $element = $form[$field_name]['widget'][0]['inline_entity_form']['email']['widget'][0]['value'];
        $ref_email = $form_state->getValue($element['#parents']);
        if ($ref_email == $this->getTicket()->get('email')->value) {
          $form_state->setError($element, 'You cannot use yourself as a referee.');
        }
      }
    }

    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    if ($this->moduleHandler->moduleExists('contacts_references')) {
      // Find any references and update their application & uid fields.
      /** @var \Drupal\contacts_references\Entity\Reference[] $references */
      $references = \Drupal::service('contacts_references.reference_field_helper')
        ->findReferences($this->entity);

      // @todo Move this to inline entity form process on reference creation.
      foreach ($references as $reference) {
        $reference->set('uid', $this->entity->getOwnerId());
        $reference->set('application', $this->entity->id());
        $reference->save();
      }
    }
    return $result;
  }

}

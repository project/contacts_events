<?php

/**
 * @file
 * Token hook implementations for Contacts Events Teams.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Implements hook_token_info_alter().
 */
function contacts_events_teams_token_info_alter(&$data) {
  $data['tokens']['contacts_ticket']['team-url'] = [
    'name' => new TranslatableMarkup('Team application URL'),
    'description' => new TranslatableMarkup('The URL of the team application page for this ticket, if applicable.'),
  ];
}

/**
 * Implements hook_tokens().
 */
function contacts_events_teams_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'contacts_ticket' && !empty($data['contacts_ticket'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'team-url':
          $replacements[$original] = '';
          if (!$data['contacts_ticket']->get('contact')->isEmpty()) {
            $url = Url::fromRoute('contacts_events_teams.application_flow',
              ['contacts_ticket' => $data['contacts_ticket']->id()],
              ['absolute' => TRUE]);
            if ($url->access($data['contacts_ticket']->get('contact')->entity)) {
              $replacements[$original] = $url->toString();
            }
          }
          break;
      }
    }
  }

  return $replacements;
}

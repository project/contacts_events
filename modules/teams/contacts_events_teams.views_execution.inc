<?php

/**
 * @file
 * Views data hook for Contacts Events Teams.
 */

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_alter().
 */
function contacts_events_teams_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() == 'contacts_events_teams_applications_current') {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    if (in_array($view->current_display, ['team', 'data_export_2'])) {
      $ticket_team_table_alias = $query->ensureTable('contacts_ticket__team');
      $team_argument = &$query->where[0]['conditions'][1];
      $pos = strpos($team_argument['field'], ' ');
      $app_team_field = substr($team_argument['field'], 0, $pos);
      $team_argument['field'] = substr_replace($team_argument['field'], "COALESCE({$app_team_field}, {$ticket_team_table_alias}.team_target_id)", 0, $pos);
    }
  }

  $app_view_ids = [
    'contacts_events_teams_applications_current',
    'contacts_events_teams_applications_archived',
  ];
  if (in_array($view->id(), $app_view_ids)) {
    $table_queue = &$query->getTableQueue();
    if (isset($table_queue['users_field_data_contacts_ticket__user__profile_crm_indiv'])) {
      $table_queue['users_field_data_contacts_ticket__user__profile_crm_indiv']['join']->extra[] = [
        'field' => 'delta',
        'value' => 0,
        'numeric' => TRUE,
      ];
    }
    else {
      \Drupal::logger('contacts_events_team')
        ->error('Unable to find profile table for view %id, possibility of duplicate records.', [
          '%id' => $view->id(),
        ]);
    }
  }
}

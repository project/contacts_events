<?php

/**
 * @file
 * Views data hook for Contacts Events Teams.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function contacts_events_teams_views_data() {
  $data = [];

  // Relationship to the team application.
  $data['contacts_ticket']['c_events_team_app']['relationship'] = [
    'title' => new TranslatableMarkup('Team applications'),
    'help' => new TranslatableMarkup('Relate team applications to the ticket it belongs to.'),
    'id' => 'standard',
    'base' => 'c_events_team_app',
    'base field' => 'ticket',
    'field' => 'id',
    'label' => new TranslatableMarkup('Application'),
  ];

  // Ticket::$is_team_ticket.
  $data['contacts_ticket__is_team_ticket']['table'] = [
    'join' => [
      'contacts_ticket' => [
        'table' => 'contacts_ticket__is_team_ticket',
        'left_field' => 'id',
        'field' => 'entity_id',
        'extra' => [
          [
            'field' => 'deleted',
            'value' => 0,
            'numeric' => TRUE,
          ],
        ],
      ],
    ],
  ];
  $data['contacts_ticket__is_team_ticket']['is_team_ticket'] = [
    'group' => new TranslatableMarkup('Ticket'),
    'title' => new TranslatableMarkup('Is team ticket'),
    'help' => new TranslatableMarkup('Whether this ticket is a team ticket.'),
    'field' => [
      'id' => 'field',
      'field_name' => 'is_team_ticket',
      'entity_type' => 'contacts_ticket',
      'real field' => 'is_team_ticket_value',
      'additional fields' => [
        'delta',
        'langcode',
        'bundle',
        'team_target_id',
      ],
      'element type' => 'div',
      'is revision' => FALSE,
      'click sortable' => TRUE,
    ],
  ];
  $data['contacts_ticket__is_team_ticket']['is_team_ticket_value'] = [
    'group' => new TranslatableMarkup('Ticket'),
    'title' => new TranslatableMarkup('Is team ticket'),
    'help' => new TranslatableMarkup('Whether this ticket is a team ticket.'),
    'argument' => [
      'id' => 'numeric',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'team_target_id',
      ],
      'field_name' => 'is_team_ticket',
      'entity_type' => 'contacts_ticket',
      'empty field name' => new TranslatableMarkup('- No value -'),
    ],
    'filter' => [
      'id' => 'boolean',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'team_target_id',
      ],
      'field_name' => 'is_team_ticket',
      'entity_type' => 'contacts_ticket',
      'allow empty' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'team_target_id',
      ],
      'field_name' => 'is_team_ticket',
      'entity_type' => 'contacts_ticket',
    ],
  ];

  // Ticket::$team.
  $data['contacts_ticket__team']['table'] = [
    'join' => [
      'contacts_ticket' => [
        'table' => 'contacts_ticket__team',
        'left_field' => 'id',
        'field' => 'entity_id',
        'extra' => [
          [
            'field' => 'deleted',
            'value' => 0,
            'numeric' => TRUE,
          ],
        ],
      ],
    ],
  ];
  $data['contacts_ticket__team']['team'] = [
    'group' => new TranslatableMarkup('Ticket'),
    'title' => new TranslatableMarkup('Team'),
    'help' => new TranslatableMarkup('The team this ticket is applying for.'),
    'field' => [
      'table' => 'contacts_ticket__team',
      'id' => 'field',
      'field_name' => 'team',
      'entity_type' => 'contacts_ticket',
      'real field' => 'team_target_id',
      'additional fields' => [
        'delta',
        'langcode',
        'bundle',
        'team_target_id',
      ],
      'element type' => 'div',
      'is revision' => FALSE,
      'click sortable' => TRUE,
    ],
    'relationship' => [
      'title' => new TranslatableMarkup('Team referenced from Ticket'),
      'label' => new TranslatableMarkup('Team'),
      'id' => 'standard',
      'base' => 'c_events_team',
      'base field' => 'id',
      'relationship field' => 'team_target_id',
    ],
  ];
  $data['contacts_ticket__team']['team_target_id'] = [
    'group' => new TranslatableMarkup('Ticket'),
    'title' => new TranslatableMarkup('Team'),
    'help' => new TranslatableMarkup('The team this ticket is applying for.'),
    'argument' => [
      'id' => 'numeric',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'team_target_id',
      ],
      'field_name' => 'team',
      'entity_type' => 'contacts_ticket',
      'empty field name' => new TranslatableMarkup('- No value -'),
    ],
    'filter' => [
      'id' => 'numeric',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'team_target_id',
      ],
      'field_name' => 'is_team_ticket',
      'entity_type' => 'contacts_ticket',
      'allow empty' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'team_target_id',
      ],
      'field_name' => 'is_team_ticket',
      'entity_type' => 'contacts_ticket',
    ],
  ];

  $data['c_events_team_app']['ce_teams_dbs_status'] = [
    'title' => t('Application DBS Status'),
    'field' => [
      'title' => t('Application DBS Status'),
      'help' => t('The DBS Status of the team application (if relevant).'),
      'id' => 'ce_teams_dbs_status',
      'real field' => 'id',
    ],
  ];

  return $data;
}

/**
 * Implements hook_views_plugins_field_alter().
 */
function contacts_events_teams_views_plugins_field_alter(array &$plugins) {
  $plugins['entity_operations']['class'] = 'Drupal\\contacts_events_teams\\Plugin\\views\\field\\EntityOperations';
}

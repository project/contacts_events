<?php

/**
 * @file
 * Install, update, uninstall and schema for the Commerce Advanced Queue module.
 */

/**
 * Implements hook_schema().
 */
function commerce_advancedqueue_schema() {
  $schema['commerce_advancedqueue_orders'] = [
    'description' => 'Stores commerce order jobs in queues.',
    'fields' => [
      'job_id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Job ID.',
      ],
      'order_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The order ID.',
      ],
      'queue_id' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The queue ID.',
      ],
      'type' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The job type.',
      ],
      'payload' => [
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'description' => 'The job payload, stored as JSON.',
      ],
      'state' => [
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => 'The job state.',
      ],
      'message' => [
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'description' => 'The job message, stored after processing the job.',
      ],
      'num_retries' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'small',
        'description' => 'The number of times the job has been retried.',
      ],
      'available' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The availability timestamp.',
      ],
      'processed' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The processing timestamp.',
      ],
      'expires' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The lease expiration timestamp.',
      ],
    ],
    'primary key' => ['job_id'],
    'indexes' => [
      'queue' => ['queue_id', 'state', 'available', 'expires'],
      'queue_state' => ['state'],
      'order_queue' => [
        'order_id',
        'queue_id',
        'state',
        'available',
        'expires',
      ],
      'order_queue_state' => ['order_id', 'state'],
    ],
  ];

  return $schema;
}

/**
 * Recreate the advanced queue table.
 */
function commerce_advancedqueue_update_8001() {
  // The previous table was botched and wont hold any meaningful data anyway.
  $schema = \Drupal::database()->schema();
  $schema->dropTable('commerce_advancedqueue_orders');
  $schema->createTable('commerce_advancedqueue_orders', [
    'description' => 'Stores commerce order jobs in queues.',
    'fields' => [
      'job_id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Job ID.',
      ],
      'order_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The order ID.',
      ],
      'queue_id' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The queue ID.',
      ],
      'type' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The job type.',
      ],
      'payload' => [
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'description' => 'The job payload, stored as JSON.',
      ],
      'state' => [
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => 'The job state.',
      ],
      'message' => [
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'description' => 'The job message, stored after processing the job.',
      ],
      'num_retries' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'small',
        'description' => 'The number of times the job has been retried.',
      ],
      'available' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The availability timestamp.',
      ],
      'processed' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The processing timestamp.',
      ],
      'expires' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The lease expiration timestamp.',
      ],
    ],
    'primary key' => ['job_id'],
    'indexes' => [
      'queue' => ['queue_id', 'state', 'available', 'expires'],
      'queue_state' => ['state'],
      'order_queue' => [
        'order_id',
        'queue_id',
        'state',
        'available',
        'expires',
      ],
      'order_queue_state' => ['order_id', 'state'],
    ],
  ]);
}

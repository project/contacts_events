<?php

namespace Drupal\commerce_advancedqueue\Plugin\AdvancedQueue\Backend;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\Plugin\AdvancedQueue\Backend\BackendInterface;
use Drupal\commerce_advancedqueue\CommerceOrderJob;

/**
 * An interface for queues that support CommerceOrderJob jobs.
 *
 * Implementations provides additional means to manage jobs for a particular
 * order and must protect against multiple jobs being processed concurrently for
 * the same order.
 */
interface CommerceOrderJobBackendInterface extends BackendInterface {

  /**
   * Gets an estimated number of jobs for an order in the queue.
   *
   * The accuracy of this number might vary.
   * On a busy system with a large number of consumers and jobs, the result
   * might only be valid for a fraction of a second and not provide an
   * accurate representation.
   *
   * @param int $order_id
   *   The order ID.
   *
   * @return array
   *   The estimated number of jobs, grouped per job status.
   *   Only the estimate for the 'queued' status is guaranteed to be present,
   *   other estimates (processing/success/failed) depend on backend
   *   capabilities and configuration.
   *
   * @see \Drupal\advancedqueue\Plugin\AdvancedQueue\Backend\BackendInterface::countJobs()
   */
  public function countJobsForOrder(int $order_id): array;

  /**
   * Claims the next available job for an order for processing.
   *
   * @param int $order_id
   *   The order ID.
   *
   * @return \Drupal\commerce_advancedqueue\CommerceOrderJob|null
   *   The job, or NULL if none available.
   *
   * @see \Drupal\advancedqueue\Plugin\AdvancedQueue\Backend\BackendInterface::claimJob()
   */
  public function claimJobForOrder($order_id): ?CommerceOrderJob;

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   *   Thrown if $job is not a \Drupal\commerce_advancedqueue\CommerceOrderJob.
   */
  public function enqueueJob(Job $job, $delay = 0);

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   *   Thrown if any of $jobs are not a
   *   \Drupal\commerce_advancedqueue\CommerceOrderJob.
   */
  public function enqueueJobs(array $jobs, $delay = 0);

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   *   Thrown if $job is not a \Drupal\commerce_advancedqueue\CommerceOrderJob.
   */
  public function retryJob(Job $job, $delay = 0);

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\commerce_advancedqueue\CommerceOrderJob|null
   *   The job, or NULL if none available.
   */
  public function claimJob(): ?CommerceOrderJob;

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   *   Thrown if $job is not a \Drupal\commerce_advancedqueue\CommerceOrderJob.
   */
  public function onSuccess(Job $job);

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   *   Thrown if $job is not a \Drupal\commerce_advancedqueue\CommerceOrderJob.
   */
  public function onFailure(Job $job);

}

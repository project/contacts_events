<?php

/**
 * @file
 * Provide views data for the Commerce Advanced Queue module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function commerce_advancedqueue_views_data() {
  $data['commerce_advancedqueue_orders'] = [];
  $data['commerce_advancedqueue_orders']['table']['group'] = t('Advanced queue');

  $data['commerce_advancedqueue_orders']['table']['base'] = [
    'field' => 'job_id',
    'title' => t('Order Jobs'),
    'help' => t('Contains a list of advanced queue jobs for Commerce Orders.'),
  ];

  $data['commerce_advancedqueue_orders']['job_id'] = [
    'title' => t('Job ID'),
    'help' => t('Primary Key: Job ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['commerce_advancedqueue_orders']['queue_id'] = [
    'title' => t('Queue ID'),
    'help' => t('The queue ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['commerce_advancedqueue_orders']['type'] = [
    'title' => t('Job type'),
    'help' => t('The job type.'),
    'field' => [
      'id' => 'advancedqueue_job_type',
    ],
    'filter' => [
      'id' => 'in_operator',
      'options callback' => '\Drupal\advancedqueue\Plugin\views\field\JobType::getOptions',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['commerce_advancedqueue_orders']['payload'] = [
    'title' => t('Payload'),
    'help' => t('The job payload, stored as JSON.'),
    'field' => [
      'id' => 'advancedqueue_json',
    ],
  ];
  $data['commerce_advancedqueue_orders']['state'] = [
    'title' => t('State'),
    'help' => t('The job state'),
    'field' => [
      'id' => 'advancedqueue_job_state',
    ],
    'filter' => [
      'id' => 'in_operator',
      'options callback' => '\Drupal\advancedqueue\Plugin\views\field\JobState::getOptions',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['commerce_advancedqueue_orders']['message'] = [
    'title' => t('Message'),
    'help' => t('The job message, stored after processing the job.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['commerce_advancedqueue_orders']['num_retries'] = [
    'title' => t('Number of retries'),
    'help' => t('The number of times the job has been retried.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['commerce_advancedqueue_orders']['available'] = [
    'title' => t('Available date'),
    'help' => t('The availability timestamp.'),
    'field' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];
  $data['commerce_advancedqueue_orders']['processed'] = [
    'title' => t('Processed date'),
    'help' => t('The processing timestamp.'),
    'field' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];
  $data['commerce_advancedqueue_orders']['expires'] = [
    'title' => t('Expire date'),
    'help' => t('The lease expiration timestamp.'),
    'field' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];
  $data['commerce_advancedqueue_orders']['operations'] = [
    'title' => t('Operations'),
    'help' => t('Provides available operations'),
    'field' => [
      'id' => 'advancedqueue_job_operations',
    ],
  ];

  $data['commerce_advancedqueue_orders']['order_id'] = [
    'title' => new TranslatableMarkup('Order ID'),
    'help' => new TranslatableMarkup('The ID of the order this job relates to.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'relationship' => [
      'base' => 'commerce_order',
      'base field' => 'order_id',
      'id' => 'standard',
      'label' => new TranslatableMarkup('Order for the Job'),
    ],
  ];

  return $data;
}

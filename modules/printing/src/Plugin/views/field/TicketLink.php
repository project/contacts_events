<?php

namespace Drupal\contacts_events_printing\Plugin\views\field;

use Drupal\contacts_events_printing\Controller\TicketPrintingController;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to the printing the users ticket.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("contacts_events_printing_ticket_link")
 */
class TicketLink extends EntityLink {

  /**
   * {@inheritdoc}
   *
   * If no ticket is found, let the booking manager go to the summary.
   */
  protected function getEntityLinkTemplate() {
    return 'booking_process';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Download');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkUrlAccess(ResultRow $row) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $booking */
    $booking = $this->getEntity($row);

    /** @var \Drupal\contacts_events\Entity\EventInterface $event */
    $event = $booking->get('event')->entity;

    $printing_enabled = TicketPrintingController::isEventPrintingEnabled($event);
    if ($printing_enabled->isForbidden()) {
      return $printing_enabled;
    }

    $url = $this->getUrlInfo($row);
    return $this->accessManager->checkNamedRoute($url->getRouteName(), $url->getRouteParameters(), $this->currentUser(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $booking */
    $booking = $this->getEntity($row);

    foreach ($booking->get('order_items')->referencedEntities() as $item) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $item */
      if ($item->bundle() !== 'contacts_ticket') {
        continue;
      }

      // Skip anonymous users to avoid no ticket holder tickets being selected.
      if (!$this->currentUser()->isAuthenticated()) {
        continue;
      }

      /** @var \Drupal\contacts_events\Entity\TicketInterface $ticket */
      $ticket = $item->getPurchasedEntity();
      if ((int) $ticket->getTicketHolderId() !== (int) $this->currentUser()->id()) {
        continue;
      }

      return $ticket->toUrl($this->options['ticket_route'])->setAbsolute($this->options['absolute']);
    }

    return parent::getUrlInfo($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['ticket_route'] = ['default' => 'booking_ticket_print'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['ticket_route'] = [
      '#type' => 'radios',
      '#title' => $this->t('Which page to show for download link.', [
        '%draft' => $this->t('Started'),
      ]),
      '#options' => [
        'booking_ticket_print' => 'Download',
        'booking_ticket' => 'View',
      ],
      '#description' => $this->t("One page will show a ticket view page while the other will download a pdf file."),
      '#default_value' => $this->options['ticket_route'],
    ];
  }

}

<?php

namespace Drupal\contacts_events_printing\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'ce_print_log' field type.
 *
 * @FieldType(
 *   id = "ce_print_log",
 *   label = @Translation("Print log"),
 *   category = @Translation("General"),
 *   default_formatter = "ce_print_log"
 * )
 */
class PrintLogItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->time !== NULL) {
      return FALSE;
    }
    elseif ($this->uid !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['time'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Printed at'));
    $properties['uid'] = DataDefinition::create('integer')
      ->setLabel(t('Printed by'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $options['time']['NotBlank'] = [];
    $options['uid']['NotBlank'] = [];
    $constraints[] = $constraint_manager->create('ComplexData', $options);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'time' => [
        'type' => 'varchar',
        'length' => 20,
      ],
      'uid' => [
        'type' => 'int',
        'size' => 'normal',
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);
    $values['time'] = gmdate('Y-m-d\TH:i:s', $timestamp);
    $values['uid'] = mt_rand(0, 1000);
    return $values;
  }

}

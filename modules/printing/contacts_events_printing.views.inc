<?php

/**
 * @file
 * Views hook implementations for Contacts Events Printing.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function contacts_events_printing_views_data() {
  $data['commerce_order']['ticket_link'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup("Download ticket link"),
    'real field' => 'order_id',
    'field' => ['id' => 'contacts_events_printing_ticket_link'],
  ];

  return $data;
}

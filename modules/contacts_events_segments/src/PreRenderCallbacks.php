<?php

namespace Drupal\contacts_events_segments;

use Drupal\contacts_events_segments\Entity\Segment;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Pre-render callbacks.
 */
class PreRenderCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'preRenderSegmentsTicketEventSegmentsCheckboxTable',
    ];
  }

  /**
   * Process callback to convert the segments into a table.
   *
   * @param array $element
   *   The checkboxes element.
   *
   * @return array
   *   The processed element.
   */
  public static function preRenderSegmentsTicketEventSegmentsCheckboxTable(array $element): array {
    $columns = [];
    $rows = [];

    // Move the segment checkboxes into place.
    foreach (Segment::loadMultiple(array_keys($element['#options'])) as $id => $segment) {
      $column = $segment->get('day')->value;
      // Track when a column is not empty.
      $columns[$column] = $column;

      $row = $segment->get('part')->value;
      $element[$id]['#description'] = $segment->get('description')->value;
      $element[$id]['#attributes']['title'] = $element[$id]['#title'];
      $element[$id]['#title_attributes']['title'] = $element[$id]['#title'];
      $element[$id]['#title'] = $element['#segment_parts'][$row];
      $rows[$row]['data'][$column]['data'] = $element[$id];
      unset($element[$id]);
    }

    $header = array_intersect_key($element['#segment_days'], $columns);

    // Build our table definition (without the header - populate this later)
    $element['table'] = [
      '#type' => 'table',
      '#weight' => -1,
      '#responsive' => FALSE,
      '#header' => $header,
      '#rows' => [],
    ];

    // Don't just set #rows on the table as the segment data doesn't come in
    // the right order, meaning the column data won't match the headings.
    // Instead generate the cell contents by looping over #segment_parts (which
    // are the row types - full day, evening etc) and then within each row
    // loop over the column headings and find the data that matches that column
    // heading.
    foreach ($element['#segment_parts'] as $row_key => $label) {
      if (isset($rows[$row_key])) {
        foreach ($element['table']['#header'] as $column => $column_label) {
          $element['table']['#rows'][$row_key]['data'][$column] = $rows[$row_key]['data'][$column] ?? [];
        }
      }
    }

    return $element;
  }

}

<?php

namespace Drupal\contacts_events_segments;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the Event Segment entity.
 *
 * @see \Drupal\contacts_events_segments\Entity\Segment
 */
class SegmentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected $viewLabelOperation = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_events_segments\Entity\SegmentInterface $entity */

    // Delete should always be forbidden for new entities.
    if ($operation == 'delete' && $entity->isNew()) {
      return AccessResult::forbidden()->addCacheableDependency($entity);
    }

    // If we don't have an event, deny access.
    /** @var \Drupal\contacts_events\Entity\EventInterface $event */
    $event = $entity->getEvent();
    if (!$event) {
      return AccessResult::forbidden('Cannot check event status.');
    }

    // Check whether day tickets are enabled.
    if ($this->areSegmentsDisabled($event)) {
      return AccessResult::forbidden('Segments are not enabled for this event.');
    }

    // View label corresponds to booking, everything else is edit.
    if ($operation == 'view_label') {
      return $event->access('book', $account, TRUE);
    }

    return $event->access('update', $account, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // Deny completely if there is no event in the context.
    if (empty($context['event'])) {
      return AccessResult::forbidden('Missing event context.');
    }

    // Check whether day tickets are enabled.
    if ($this->areSegmentsDisabled($context['event'])) {
      return AccessResult::forbidden('Segments are not enabled for this event.');
    }

    // Creating a segment requires event edit permission.
    return $context['event']->access('update', $account, TRUE);
  }

  /**
   * Check whether segments are disabled on the event.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event to check.
   *
   * @return bool
   *   TRUE if segments are disabled.
   */
  protected function areSegmentsDisabled(EventInterface $event): bool {
    return $event->getSetting('segments', EventInterface::STATUS_DISABLED) == EventInterface::STATUS_DISABLED;
  }

}

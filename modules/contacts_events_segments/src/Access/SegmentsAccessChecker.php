<?php

namespace Drupal\contacts_events_segments\Access;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Checks if segments are enabled for the event.
 */
class SegmentsAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * This only checks whether segments are disabled on the event.
   * and is only used to check whether the configuration UI for segments is
   * enabled as a route access check in
   * Drupal\contacts_events_segments\SegmentHtmlRouteProvider.
   *
   * It intentionally doesn't take into account whether segments are
   * closed vs open (which requires a check of whether the current user is
   * a member of staff), so shouldn't be used to show the
   * public facing ticket creation UI for delegates.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event we are checking segments access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function configAccess(EventInterface $contacts_event) {
    $result = AccessResult::allowedIf($contacts_event->getSetting('segments', EventInterface::STATUS_DISABLED) != EventInterface::STATUS_DISABLED)
      ->addCacheableDependency($contacts_event);
    if ($result instanceof AccessResultReasonInterface) {
      $result->setReason('Segments are not enabled for this event.');
    }
    return $result->andIf($contacts_event->access('update', NULL, TRUE));
  }

  /**
   * Checks whether day tickets can be selected via the front-end UI.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The current event.
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The user being checked to see if they can access day tickets.
   * @param bool $return_as_object
   *   Whether to return the result as an AccessResult (TRUE) or as a boolean
   *   (FALSE). Defaults to FALSE.
   *
   * @return bool|\Drupal\Core\Access\AccessResult
   *   Whether the user has access to add day tickets.
   */
  public function bookingAccess(EventInterface $contacts_event, AccountInterface $user = NULL, $return_as_object = FALSE) {
    if ($user == NULL) {
      $user = \Drupal::currentUser();
    }

    $setting = $contacts_event->getSetting('segments', EventInterface::STATUS_DISABLED);

    $result = AccessResult::allowedIf($setting == EventInterface::STATUS_OPEN || ($setting == EventInterface::STATUS_CLOSED && in_array('staff', $user->getRoles())))
      ->addCacheableDependency($contacts_event)
      ->addCacheableDependency($user);

    if ($return_as_object) {
      return $result;
    }

    return $result->isAllowed();
  }

}

<?php

namespace Drupal\contacts_events_segments\Entity;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an event segment entity type.
 */
interface SegmentInterface extends ContentEntityInterface {

  /**
   * Whether the segment is enabled.
   *
   * @return bool
   *   TRUE if the segment is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Get the event for this segment.
   *
   * @return \Drupal\contacts_events\Entity\EventInterface
   *   The event.
   */
  public function getEvent(): EventInterface;

  /**
   * Get the ID of the event for this segment.
   *
   * @return int
   *   The event ID.
   */
  public function getEventId(): int;

}

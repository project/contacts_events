<?php

namespace Drupal\contacts_events_segments\Entity;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the event segment entity class.
 *
 * @ContentEntityType(
 *   id = "contacts_event_segment",
 *   label = @Translation("Event Segment"),
 *   label_collection = @Translation("Event Segments"),
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_events_segments\SegmentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\contacts_events_segments\SegmentAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\contacts_events_segments\Form\SegmentForm",
 *       "edit" = "Drupal\contacts_events_segments\Form\SegmentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_segments\SegmentHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "contacts_event_segment",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "collection" = "/event/{contacts_event}/segments",
 *     "add-form" = "/event/{contacts_event}/segments/add",
 *     "canonical" = "/event/{contacts_event}/segments/{contacts_event_segment}",
 *     "edit-form" = "/event/{contacts_event}/segments/{contacts_event_segment}",
 *     "delete-form" = "/event/{contacts_event}/segments/{contacts_event_segment}/delete",
 *   },
 * )
 */
class Segment extends ContentEntityBase implements SegmentInterface {

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvent(): EventInterface {
    return $this->get('event')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventId(): int {
    return $this->get('event')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    if (!in_array($rel, ['collection', 'add-page', 'add-form'], TRUE)) {
      // The event entity ID is needed as a route parameter.
      $uri_route_parameters['contacts_event'] = $this->get('event')->target_id;
    }
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setDescription(new TranslatableMarkup('The title for this segment.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Event'))
      ->setDescription(new TranslatableMarkup('The Event'))
      ->setSetting('target_type', 'contacts_event')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDescription(new TranslatableMarkup('The description for this segment.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Status'))
      ->setDescription(new TranslatableMarkup('Allow this segment to be booked, subject to event settings.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}

<?php

namespace Drupal\contacts_events_segments\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Form controller for the event segment entity edit forms.
 */
class SegmentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    /** @var \Drupal\contacts_events_segments\Entity\SegmentInterface $entity */
    $entity = parent::getEntityFromRouteMatch($route_match, $entity_type_id);
    if ($entity->isNew() && $entity->get('event')->isEmpty()) {
      $entity->set('event', $route_match->getRawParameter('contacts_event'));
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_events_segments\Entity\SegmentInterface $entity */
    $entity = $this->getEntity();
    $result = $entity->save();

    $message_arguments = [
      '%event' => $entity->getEvent()->label(),
      '%label' => $entity->label(),
    ];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('%label for %event has been created.', $message_arguments));
    }
    else {
      $this->messenger()->addStatus($this->t('%label for %event has been updated.', $message_arguments));
    }

    $form_state->setRedirect('entity.contacts_event_segment.collection', [
      'contacts_event' => $entity->getEventId(),
    ]);
  }

}

let villageAllageAllocationUi = `
<div id="village-allocation-container">

  <p>To allocate bookings to villages, either drag and drop the booking(s) into the villages, or select multiple bookings using the checkboxes and click the "Allocate Selected Bookings" button</p>

  <ul class="action-links">
    <li>
      <a href="javascript:void(0)" @click="showAllocateDialog = true" class="button button-action button--primary button--small">Allocate Selected Bookings</a>
      <a href="javascript:void(0)" @click="showDeallocateDialog = true" class="button button-action button--primary button--small">Deallocate Selected Bookings</a>
    </li>
  </ul>

  <div style="display: flex">
    <div id="facets-container">
      <h2>Camping Group Search</h2>
      <facets :facets="facets" :filters="filters" @load-bookings="loadBookings" @facet-search="facetSearch"></facets>
    </div>

    <div id="bookings-container">
      <div id="overlay" class="loading" v-show="loading"></div>

      <h2>
        Bookings
        <village-filter :villages="villages" :filters="filters" @load-bookings="loadBookings" />
      </h2>

      <div>
        <group v-for="group in groups"
               :group="group"
               :villages="villages"
               :key="group.id"
               @booking-selected="bookingSelected"
               @booking-deselected="bookingDeselected"
               @linked-selected="linkedSelected"
               @linked-deselected="linkedDeselected"

        />
        <pager :page-number="pageNumber" :total-pages="totalPages" @navigate="pagerNavigate"></pager>
      </div>

    </div>

    <div id="villages-container">
      <h2>Villages</h2>
      <village v-for="v in villages" :village="v" :name="v.name" :key="v.id" @drop="villageDropped"></village>
    </div>
  </div>

  <modal v-if="showAllocateDialog">
    <div slot="header">Allocating {{selectedBookings.length}} groups</div>

    Village: <select v-model="villageToAllocate">
      <option value="">Please select...</option>
      <option v-for="village in villages" v-text="village.name" :value="village"></option>
    </select>
    <button slot="footer" @click="allocate()">Allocate to Village</button>
    <button slot="footer" class="modal-default-button" @click="showAllocateDialog = false">Cancel</button>
  </modal>

  <modal v-if="showDeallocateDialog">
    <div slot="header">De-allocating {{bookingsToDeallocate.length}} bookings</div>

    <ul>
      <li v-for="booking in bookingsToDeallocate">
        {{booking.orderNumber}}
      </li>
    </ul>

    <button slot="footer" @click="deallocate()">Deallocate</button>
    <button slot="footer" class="modal-default-button" @click="showDeallocateDialog = false">Cancel</button>
  </modal>

  <modal v-if="error">
    <span slot="header">An error has occurred</span>
    <pre>{{error}}</pre>
    <button slot="footer" @click="clearError()">Close</button>
  </modal>
</div>
`;

Vue.component('group', {
  template: `
              <div class="village-group">
                <h3>
                  <input type="checkbox" :checked="isGroupSelected" @click="selectGroup" />

                  <a v-if="group.links.length"
                    class="linked"
                    :title="linkText"
                    @click.prevent="selectLinked"
                    href="#"
                   >Linked</a>

                  {{group.name}}

                  <small>
                    Pitches: {{group.pitches}}
                  </small>

                  <small>
                    Allocated to: {{getAllocatedVillages(group)}}
                  </small>
                </h3>

                <div class="group-bookings">
                  <booking v-for="(booking, index) in group.bookings"
                           :booking="booking"
                           :group-id="group.id"
                           :is-alternate="index % 2 === 0"
                           :key="booking.id"
                           @booking-selected="bookingSelected(booking)"
                           @booking-deselected="bookingDeselected(booking)"></booking>
                </div>
              </div>`,


  props: ['group', 'villages'],

  computed: {
    isGroupSelected() {
      return this.group.bookings.length > 0 && this.group.bookings.every(b => b.selected);
    },
    linkText() {
      return 'Linked to ' + this.group.links.map(l => l.name).join(', ');
    }
  },

  methods: {
    selectGroup() {
      if (this.isGroupSelected) {
        // deselect all.
        for (let booking of this.group.bookings) {
          if (booking.selected) {
            booking.selected = false;
          }
        }
      }
      else {
        // Select any unselected.
        for(let booking of this.group.bookings) {
          if (!booking.selected) {
            booking.selected = true;
          }
        }
      }
    },

    selectLinked() {
      this.selectGroup();

      if (this.isGroupSelected) {
        this.$emit('linked-selected', this.group.links.map(l => l.id));
      }
      else {
        this.$emit('linked-deselected', this.group.links.map(l => l.id));
      }
    },

    getAllocatedVillages(group) {
      return group.villages.map(vid => this.getVillageName(vid)).join(', ') || '-';
    },

    getVillageName(id) {
      let match = this.villages.find(v => v.id == id);
      return match ? match.name : '';
    },

    bookingSelected(booking) {
      // Just pass it up to the parent.
      this.$emit('booking-selected', booking);
    },

    bookingDeselected(booking) {
      // Just pass it up to the parent.
      this.$emit('booking-deselected', booking);
    }
  }
});

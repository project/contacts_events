Vue.component('facets', {

  template: `<div>
              <div class="facet group-name-search">
                <form @submit.prevent="emitSearchEvent" action="">
                  <input type="text" class="form-text" placeholder="Search" v-model="filters.searchText" />
                  <button class="button" @click="emitSearchEvent" type="submit">Search</button>
                </form>
                <a href="javascript:void(0)" @click="clearFilters">Clear all filters</a>
              </div>

              <div class="facet">
                <h3>Has Requirements</h3>
                <select v-model="filters.hasRequirements" @change="$emit('load-bookings')">
                  <option value="All">- Any -</option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
              </div>

              <div class="facet">
                <h3>Hide Zero Pitches</h3>
                <select v-model="filters.hideZeroPitches" @change="$emit('load-bookings')">
                  <option value="1">Yes</option>
                  <option value="All">No</option>
                </select>
              </div>


              <div v-for="facet in facets" class="facet" :key="facet.id">
                <h3>{{facet.name}}</h3>

                <ul>
                  <li v-for="item in facet.items" :class="{'facet-active': isActive(item) }">
                    <a href="javascript:void(0)" @click="toggle(item)">{{item.name}}
                      <span v-if="item.showCount" class="facet-count">{{item.count}}</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>`,

  props: ['facets', 'filters'],
  data() {
    return {
      activeItems: [],
    }
  },
  methods: {
    // Checks whether an individual facet filter item (within a facet)
    // is already active.
    isActive(item) {
      return this.activeItems.includes(item.id);
    },

    // Toggles an individual facet item on or off.
    toggle(item) {
      // Use IDs rather than references to the facets
      // to store active state, as reloading booking list also reloads facets.
      // If we track active facets by reference, then this won't work properly
      // as loading the bookings will swap out the facet references.
      let index = this.activeItems.indexOf(item.id);

      if (index > -1) {
        this.activeItems.splice(index, 1);
      }
      else {
        this.activeItems.push(item.id);
      }

      this.emitSearchEvent();
    },

    clearFilters() {
      this.activeItems  = [];
      this.filters.searchText = null;
      this.filters.hideZeroPitches = '1';
      this.filters.hasRequirements = 'All';
      this.emitSearchEvent();
    },

    emitSearchEvent() {
      let facets = this.activeItems.map(fid => this.getFacetItemById(fid).filter);

      if (this.searchText && this.searchText.trim()) {
        facets.push("camping_group_name:" + this.searchText);
      }

      this.$emit('facet-search', facets);
    },

    getFacetItemById(id) {
      for(let facet of this.facets) {
        let match = facet.items.find(f => f.id == id);

        if (match) {
          return match;
        }
      }
      return null;
    },
  }
});

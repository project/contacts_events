Vue.component('pager', {
  props: ['pageNumber', 'totalPages'],

  data() {
    return {
      showPreviousEllipses: false,
      showNextEllipses: false,
    }
  },

  computed: {
    // The current pager is the page we are currently paged to.
    pagerCurrent() { return this.pageNumber + 1; },

    // The first pager is the first page listed by this pager piece (re quantity).
    pagerFirst() { return this.pagerCurrent() - this.pagerMiddle + 1; },

    // The last is the last page listed by this pager piece (re quantity).
    pagerLast() { return this.pagerCurrent() + this.quantity - this.pagerMiddle; }
  },

  methods: {
    calculatePages() {
      let pages = [];
      // How many pages to show before it generates ellipses.
      let quantity = 9;
      let pagerMiddle = Math.ceil(quantity / 2);
      let pagerCurrent = this.pageNumber + 1;
      let pagerFirst = pagerCurrent - pagerMiddle;
      let pagerLast = pagerCurrent + quantity + pagerMiddle;
      let pagerMax = this.totalPages;

      let i = pagerFirst;

      if (pagerLast > this.totalPages) {
        i = i + (pagerMax - pagerLast);
        pagerLast = pagerMax;
      }
      if (i <= 0) {
        // Adjust "center" if at start of query.
        pagerLast = pagerLast + (1 - i);
        i = 1;
      }
      if (i !== pagerMax) {
        if (i > 1) {
          this.showPreviousEllipses = true;
        }

        for (; i <= pagerLast && i <= pagerMax; i++) {
          pages.push(i);
        }

        if (i < pagerMax + 1) {
          this.showNextEllipses = true;
        }
      }
      return pages;
    }
  },

  template: `<nav class="pager">
    <ul class="pager__items js-pager__items">
        <li v-if="pageNumber > 0" class="pager__item pager__item--first" >
          <a href="javascript:void(0)" @click="$emit('navigate', 0)">« First</a>
        </li>
        
        <li v-if="pageNumber > 0" class="pager__item pager__item--previous">
          <a href="javascript:void(0)" @click="$emit('navigate', pageNumber-1)">‹ Previous</a>
        </li>
        
        <li v-if="showPreviousEllipses" class="pager__item pager__item--ellipsis"></li>
        
        <li v-for="page in calculatePages()" :key="page" :class="{ 'pager__item': true, 'is-active': page-1 == pageNumber }">
          <a href="javascript:void(0)" @click="$emit('navigate', page - 1)">{{page}}</a>
        </li>
        
        <li v-if="showNextEllipses" class="pager__item pager__item--ellipsis">&hellip;</li>

        <li v-if="pageNumber < (totalPages - 1)" class="pager__item pager__item--next">
          <a href="javascript:void(0)" @click="$emit('navigate', pageNumber+1)">Next ›</a>
        </li>
        
         <li v-if="pageNumber < (totalPages - 1)" class="pager__item pager__item--last">
           <a href="javascript:void(0)" @click="$emit('navigate', totalPages-1)">Last »</a>
         </li>
    </ul>
  </nav>`,
});

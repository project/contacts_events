Vue.component('village', {
  props: ['village'],
  template: `
    <div :class="classes" @drop.prevent="drop" @dragover.prevent="dragOver"  @dragleave="dragLeave">
      <h3 :style="{ color: village.colour }">
        {{village.name}}
      </h3>

      <div class="special-requirements">
        <span v-for="requirement in village.specialRequirements"
              :title="requirement.name"
              :class="['requirements', 'requirements-' + requirement.icon]">{{requirement.name}}</span>
      </div>

      <div>Allocated {{village.pitchesAllocated}} of {{village.pitches}}</div>
    </div>`,

  data() {
    return {
      isDropTarget: false,
    }
  },
  computed: {
    classes() {
      let classes = ['village', this.fillStatus()];
      // Dynamically add the 'drop-active' css class if this village
      // is currently being hovered over during a drag-drop operation
      if (this.isDropTarget) {
        classes.push('drop-active');
      }
      return classes;
    },
  },
  methods: {
    // When user drags some bookings over the village, set isDropTaret
    // so we can use css to give a visual indicator that we can drop here.
    dragOver(event) {
      this.isDropTarget = true;
    },

    // When the user's mouse leaves the village, ensure this is no longer
    // the active target.
    dragLeave(event) {
      this.isDropTarget = false;
    },

    // When the user drops some bookings onto this village, send a message
    // back up to the parent component to handle the allocation.
    drop(event) {
      this.isDropTarget = false;
      this.$emit('drop', this.village);
    },

    // Calculate the village fill status, used for colouring the village text.
    fillStatus() {
      let fillValue = this.village.fillValue / 100;

      if (this.village.pitchesAllocated < (this.village.pitches * fillValue)) {
        return 'space';
      }
      else if (this.village.pitchesAllocated < this.village.pitches) {
        return 'near';
      }
      return 'full';
    }
  }
});

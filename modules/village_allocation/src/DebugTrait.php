<?php

namespace Drupal\village_allocation;

use Drupal\Component\Render\MarkupInterface;

/**
 * Trait with debugging methods.
 *
 * If devel is enabled, methods logged with this trait will be passed through
 * the devel dumper. If devel is not enabled, then no message will be output.
 *
 * @package Drupal\village_allocation
 */
trait DebugTrait {
  /**
   * The devel dumper manager.
   *
   * @var \Drupal\devel\DevelDumperManagerInterface
   */
  protected $dumper;

  /**
   * Output some debug information.
   *
   * @param mixed $input
   *   The variable to dump.
   * @param string $name
   *   (optional) The label to output before variable, defaults to NULL.
   */
  protected function debug($input, $name = NULL) {
    if (!isset($this->dumper)) {
      $container = \Drupal::getContainer();
      if ($container->has('devel.dumper')) {
        $this->dumper = $container->get('devel.dumper');
      }
      else {
        $this->dumper = FALSE;
      }
    }

    if ($this->dumper) {
      if ($input instanceof MarkupInterface) {
        $input = (string) $input;
      }
      $this->dumper->message($input, $name);
    }
  }

}

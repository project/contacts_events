<?php

namespace Drupal\Tests\village_allocation;

use Drupal\devel\DevelDumperManagerInterface;

/**
 * Test devel dumper that just prints messages to the console.
 *
 * @package Drupal\Tests\village_allocation
 */
class ConsoleDumper implements DevelDumperManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function dump($input, $name = NULL, $plugin_id = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function export($input, $name = NULL, $plugin_id = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function message($input, $name = NULL, $type = 'status', $plugin_id = NULL) {
    print $input;
    print "\n";
  }

  /**
   * {@inheritdoc}
   */
  public function debug($input, $name = NULL, $plugin_id = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function dumpOrExport($input, $name = NULL, $export = TRUE, $plugin_id = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function exportAsRenderable($input, $name = NULL, $plugin_id = NULL) {

  }

}

<?php

namespace Drupal\contacts_events_villages\Access;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks if village groups and accommodation are enabled for a given event.
 */
class VillageGroupAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event we are checking village group access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(EventInterface $contacts_event) {
    // Check that accommodation is configured.
    $result = AccessResult::allowedIf($contacts_event->hasField('accommodation_types') && $contacts_event->get('accommodation_types')->count());

    if ($result->isAllowed()) {
      $accommodation_types = array_map(function ($item) {
        return $item['target_id'];
      }, $contacts_event->get('accommodation_types')->getValue());

      // Check if 'camping' is in accommodation options.
      $result = $result->andIf(AccessResult::allowedIf(in_array('camping', $accommodation_types)));

      // Check if village groups are configured.
      $result = $result->andIf(AccessResult::allowedIf($contacts_event->hasField('village_group_types') && $contacts_event->get('village_group_types')->count()));
    }

    return $result->addCacheableDependency($contacts_event);
  }

}

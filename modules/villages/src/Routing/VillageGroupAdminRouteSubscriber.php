<?php

namespace Drupal\contacts_events_villages\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Router Subscriber to make Village Group view an admin route.
 *
 * @package Drupal\contacts_events_villages\EventSubscriber
 */
class VillageGroupAdminRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('view.contacts_events_village_groups.page_1')) {
      $route->setOption('_admin_route', TRUE);
    }
  }

}

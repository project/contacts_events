<?php

namespace Drupal\contacts_events_villages\Plugin\views\filter;

use Drupal\contacts_events_villages\Entity\VillageGroup;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Filter handler for camping groups.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("contacts_events_order_camping")
 */
class CampingGroup extends InOperator {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $groups = $this->value ? VillageGroup::loadMultiple($this->value) : [];
    $default_value = EntityAutocomplete::getEntityLabels($groups);

    $form['value'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Camping Location'),
      '#description' => $this->t('Start typing the name of the group or its ID.'),
      '#target_type' => 'c_events_village_group',
      '#selection_handler' => 'village_groups_for_event:c_events_village_group',
      '#tags' => FALSE,
      '#default_value' => $default_value,
      '#process_default_value' => $this->isExposed(),
    ];

    if (isset($this->view->argument['event_target_id'])) {
      // View arguments are keyed by their id. We need to get their numerical
      // position to obtain the value.
      $index = array_search('event_target_id', array_keys($this->view->argument));
      $value = $this->view->args[$index];
      $form['value']['#selection_settings']['event_id'] = $value;
    }

    $user_input = $form_state->getUserInput();
    if ($form_state->get('exposed') && !isset($user_input[$this->options['expose']['identifier']])) {
      $user_input[$this->options['expose']['identifier']] = $default_value;
      $form_state->setUserInput($user_input);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateExposed(&$form, FormStateInterface $form_state) {
    if (empty($this->options['exposed'])) {
      return;
    }

    if (empty($this->options['expose']['identifier'])) {
      return;
    }

    $identifier = $this->options['expose']['identifier'];
    $input = $form_state->getValue($identifier);

    if ($this->options['is_grouped'] && isset($this->options['group_info']['group_items'][$input])) {
      $this->operator = $this->options['group_info']['group_items'][$input]['operator'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    return $this->valueOptions = [];
  }

}

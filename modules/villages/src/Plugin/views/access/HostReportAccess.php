<?php

namespace Drupal\contacts_events_villages\Plugin\views\access;

use Drupal\contacts_events_villages\Controller\VillageHostInfoController;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides access controls for the host report.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "host_report",
 *   title = @Translation("Host Report"),
 *   help = @Translation("Access will be granted to users who can view the host report.")
 * )
 */
class HostReportAccess extends AccessPluginBase {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $resolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->resolver = $container->get('class_resolver');
    $plugin->entityTypeManager = $container->get('entity_type.manager');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    /** @var \Drupal\contacts_events_villages\Controller\VillageHostInfoController $controller */
    $controller = $this->resolver->getInstanceFromDefinition(VillageHostInfoController::class);

    // Attempt to find the arguments.
    $args = $this->view->args ?: $this->view->element['#arguments'] ?: NULL;
    if (!$args) {
      return AccessResult::forbidden('Unable to find event and village.');
    }

    // Load the event and village for the access check.
    $event = $this->entityTypeManager->getStorage('contacts_event')->load($args[0]);
    $village = $this->entityTypeManager->getStorage('c_events_village')->load($args[1]);
    return $controller->access($event, $village);
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_custom_access', VillageHostInfoController::class . '::access');

    $parameters = $route->getOption('parameters') ?? [];
    $parameters['contacts_event'] = ['type' => 'entity:contacts_event'];
    $parameters['village'] = ['type' => 'entity:c_events_village'];
    $route->setOption('parameters', $parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('Host Report');
  }

}

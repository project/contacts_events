<?php

namespace Drupal\contacts_events_villages\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;

/**
 * Order balance views field hander.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("contacts_events_village_requirements_processed")
 */
class VillageRequirementsProcessedField extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    // No-op.
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;

    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_order\Entity\Order $order */
      $order = $this->getEntity($row);

      $form[$this->options['id']][$row_index] = [
        '#type' => 'checkbox',
        '#default_value' => $order->get('village_requirements_processed')->value,
      ];
    }
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $all_selected = $form_state->getValue($this->options['id'], []);

    foreach ($all_selected as $row_index => $selected) {
      /** @var \Drupal\commerce_order\Entity\Order $order */
      $order = $this->getEntity($this->view->result[$row_index]);

      if ($order->get('village_requirements_processed')->value != $selected) {
        $order->set('village_requirements_processed', $selected);
        $order->save();
      }
    }
  }

}

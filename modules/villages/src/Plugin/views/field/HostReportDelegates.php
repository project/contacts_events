<?php

namespace Drupal\contacts_events_villages\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Host Report - Delegates field handler.
 *
 * @ViewsField("contacts_events_villages_host_report_delegates")
 */
class HostReportDelegates extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return [
      '#type' => 'view',
      '#name' => 'contacts_events_village_hosts_report_tickets',
      '#arguments' => [$this->getValue($values)],
    ];
  }

}

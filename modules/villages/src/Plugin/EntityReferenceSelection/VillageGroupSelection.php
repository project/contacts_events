<?php

namespace Drupal\contacts_events_villages\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Ensures only active teams are available for selection.
 *
 * @EntityReferenceSelection(
 *   id = "village_groups_for_event:c_events_village_group",
 *   label = @Translation("Active Team selection"),
 *   entity_types = {"c_events_village_group"},
 *   group = "village_groups",
 *   weight = 1
 * )
 */
class VillageGroupSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    // If we're embedded on a page that makes use of the current event,
    // then add this as a filter so we only get camping groups for this event.
    // See CampingGroup::valueForm for where this is populated.
    $configuration = $this->getConfiguration();
    if (isset($configuration['event_id'])) {
      $query->condition('event', $configuration['event_id']);
    }
    return $query;
  }

}

<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Camping village group entity.
 *
 * @ingroup contacts_events_villages
 *
 * @ContentEntityType(
 *   id = "c_events_village",
 *   label = @Translation("Camping village"),
 *   label_collection = @Translation("Camping villages"),
 *   label_singular = @Translation("camping village"),
 *   label_plural = @Translation("camping villages"),
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_events_villages\VillageListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\contacts_events_villages\Form\VillageForm",
 *       "add" = "Drupal\contacts_events_villages\Form\VillageForm",
 *       "edit" = "Drupal\contacts_events_villages\Form\VillageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\contacts_events_villages\VillageAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_villages\VillageHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "c_events_village",
 *   admin_permission = "manage c_events_village entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/event/{contacts_event}/accommodation/villages/{c_events_village}",
 *     "add-form" = "/event/{contacts_event}/accommodation/villages/add",
 *     "edit-form" = "/event/{contacts_event}/accommodation/villages/{c_events_village}/edit",
 *     "delete-form" = "/event/{contacts_event}/accommodation/villages/{c_events_village}/delete",
 *     "collection" = "/event/{contacts_event}/accommodation/villages",
 *   }
 * )
 */
class Village extends ContentEntityBase {

  /**
   * Gets the event associated with this village.
   *
   * @return \Drupal\contacts_events\Entity\EventInterface
   *   The event.
   */
  public function getEvent() : EventInterface {
    return $this->get('event')->entity;
  }

  /**
   * Sets the event associated with the village.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event.
   */
  public function setEvent(EventInterface $event) {
    $this->set('event', $event);
  }

  /**
   * Gets the ID of the event associated with this village.
   *
   * @return int|null
   *   Event ID.
   */
  public function getEventId() {
    return $this->get('event')->target_id;
  }

  /**
   * Gets the village name.
   *
   * @return string
   *   The name of the village.
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $params = parent::urlRouteParameters($rel);
    $params['contacts_event'] = $this->getEventId();
    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the village.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Event'))
      ->setDescription(t('The event this village is for.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'contacts_event')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pitches'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('No. Available pitches'))
      ->setDescription(t('The name of the village.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fill_value'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Fill value'))
      ->setDescription(t('Max percentage this village can be filled'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['colour'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Colour'))
      ->setSetting('allowed_values', [
        '#1360A6' => new TranslatableMarkup('Blue'),
        '#D5198A' => new TranslatableMarkup('Pink'),
        '#C78912' => new TranslatableMarkup('Gold'),
        '#E8C020' => new TranslatableMarkup('Yellow'),
        '#446F42' => new TranslatableMarkup('Green'),
        '#6C2C8D' => new TranslatableMarkup('Purple'),
        '#92582A' => new TranslatableMarkup('Brown'),
        '#CE2F33' => new TranslatableMarkup('Red'),
        '#F67D22' => new TranslatableMarkup('Orange'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['special_requirements'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Special Requirements'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['village_requirements']])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['notes'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Notes'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['gate'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Gate'))
      ->setDescription(t('The entrance gate for this village'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('Indicates the order of the village.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['village_host_info'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Camp Site Host Info'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['village_host_files'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Camp Site Host Files'))
      ->setSettings([
        'file_extensions' => 'txt rtf doc docx ppt pptx xls xlsx pdf odf odg odp ods odt fodt fods fodp fodg key numbers pages',
        'uri_scheme' => 'private',
        'file_directory' => 'village-host/[date:custom:Y]-[date:custom:m]',
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}

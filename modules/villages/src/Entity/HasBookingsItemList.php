<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Calculates whether a group has any bookings.
 *
 * @package Drupal\contacts_events_villages\Entity
 */
class HasBookingsItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $entity */
    $entity = $this->getEntity();

    $query = \Drupal::entityQuery('commerce_order');
    $query->accessCheck(FALSE);
    $query->condition('village_group.target_id', $entity->id());
    $query->condition('state', 'draft', '<>');
    $query->count();
    $count = $query->execute();

    $this->list[0] = $this->createItem(0, $count > 0);
  }

  /**
   * Reset the computed value.
   */
  public function resetValue() {
    $this->valueComputed = FALSE;
  }

}

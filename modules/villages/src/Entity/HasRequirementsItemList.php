<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Calculates whether a group has any bookings that have special requirements.
 *
 * @package Drupal\contacts_events_villages\Entity
 */
class HasRequirementsItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $entity */
    $entity = $this->getEntity();

    $count = \Drupal::entityQuery('commerce_order')
      ->accessCheck(FALSE)
      ->condition('village_group.target_id', $entity->id())
      ->condition('state', 'draft', '<>')
      ->exists('village_requirements')
      ->count()
      ->execute();

    $this->list[0] = $this->createItem(0, $count > 0);
  }

  /**
   * Reset the computed value.
   */
  public function resetValue() {
    $this->valueComputed = FALSE;
  }

}

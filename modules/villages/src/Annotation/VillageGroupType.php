<?php

namespace Drupal\contacts_events_villages\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Village group type item annotation object.
 *
 * @see \Drupal\contacts_events_villages\Plugin\VillageGroupTypeManager
 * @see plugin_api
 *
 * @Annotation
 */
class VillageGroupType extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}

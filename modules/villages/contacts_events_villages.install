<?php

/**
 * @file
 * Install, update and uninstall functions for the camping villages.
 */

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Implements hook_install().
 */
function contacts_events_villages_install() {
  $village_group = [
    'settings' => [],
    'third_party_settings' => [],
    'type' => "village_group_camping",
    'weight' => 2,
    'region' => "content",
  ];
  /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface[] $displays */
  $displays = EntityFormDisplay::loadMultiple([
    'commerce_order.contacts_booking.booking_accom_camping',
    'commerce_order.contacts_booking.booking_admin_accom_camping',
  ]);
  foreach ($displays as $display) {
    $display
      ->setComponent('village_group', $village_group)
      ->save();
  }

  /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $event_display */
  $event_display = EntityFormDisplay::load('contacts_event.default.default');
  if ($event_display && $field_group = $event_display->getThirdPartySetting('field_group', 'group_booking_settings')) {
    $field_group['children'][] = 'village_group_types';
    $event_display->setThirdPartySetting('field_group', 'group_booking_settings', $field_group);
    $event_display->save();
  }
}

/**
 * Adds the sorting code field to VillageGroup.
 */
function contacts_events_villages_update_8001() {
  $field = BaseFieldDefinition::create('string')
    ->setLabel(t('Sorting code'))
    ->setDescription(t('The value to sort on for automatic allocation. Typically a postcode'));

  \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition('allocation_sorting_code', 'c_events_village_group', 'contacts_events_villages', $field);
}

/**
 * Install the Village entity.
 */
function contacts_events_villages_update_8002() {
  $entity_type_manager = Drupal::entityTypeManager();
  $update_manager = \Drupal::entityDefinitionUpdateManager();
  $update_manager->installEntityType($entity_type_manager->getDefinition('c_events_village'));
}

/**
 * Additional fields for events.
 */
function contacts_events_villages_update_8003() {
  $fields['village_host_info'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Camp Site Host Info'))
    ->setRequired(FALSE)
    ->setDisplayOptions('view', [
      'region' => 'hidden',
    ])
    ->setDisplayOptions('form', [
      'type' => 'text_textarea',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', FALSE);

  $fields['village_host_files'] = BaseFieldDefinition::create('file')
    ->setLabel(t('Camp Site Host Files'))
    ->setSettings([
      'file_extensions' => 'txt rtf doc docx ppt pptx xls xlsx pdf odf odg odp ods odt fodt fods fodp fodg key numbers pages',
      'uri_scheme' => 'private',
      'file_directory' => 'village-host/[date:custom:Y]-[date:custom:m]',
    ])
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setRequired(FALSE)
    ->setDisplayOptions('view', [
      'region' => 'hidden',
    ])
    ->setDisplayOptions('form', [
      'type' => 'default',
    ])
    ->setDisplayConfigurable('view', FALSE)
    ->setDisplayConfigurable('form', TRUE);

  foreach ($fields as $name => $field) {
    \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition($name, 'c_events_village', 'contacts_events_villages', $field);
  }
}

/**
 * Increase field length.
 */
function contacts_events_villages_update_8004() {
  $database = \Drupal::database();
  $database->query('alter table c_events_village_group modify name varchar(255)');

  $storage_key = 'c_events_village_group.field_schema_data.name';
  $storage_schema = \Drupal::keyValue('entity.storage_schema.sql');
  $field_schema = $storage_schema->get($storage_key);
  $field_schema['c_events_village_group']['fields']['name']['length'] = 255;
  $storage_schema->set($storage_key, $field_schema);
}

<?php

/**
 * @file
 * Views data for the Contacts Events Accommodation module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function contacts_events_accommodation_views_data() {
  $data = [];

  $data['commerce_order']['accommodation'] = [
    'group' => new TranslatableMarkup('Booking'),
    'title' => new TranslatableMarkup('Accommodation'),
    'field' => [
      'id' => 'contacts_events_accommodation_booking_accommodation',
      'real field' => 'order_id',
    ],
  ];

  return $data;
}

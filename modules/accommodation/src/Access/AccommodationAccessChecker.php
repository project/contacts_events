<?php

namespace Drupal\contacts_events_accommodation\Access;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks if passed parameter matches the route configuration.
 */
class AccommodationAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event we are checking accommodation access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(EventInterface $contacts_event) {
    return AccessResult::allowedIf($contacts_event->hasField('accommodation_types') && $contacts_event->get('accommodation_types')->count())
      ->addCacheableDependency($contacts_event);
  }

}

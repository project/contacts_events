<?php

namespace Drupal\contacts_events_accommodation\EventSubscriber;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Event\CloneEvent;
use Drupal\contacts_events\EventSubscriber\CloneEventDependentEntitySubscriberBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Accommodation clone event subscriber.
 */
class AccommodationCloneEventSubscriber extends CloneEventDependentEntitySubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function onClone(CloneEvent $event): void {
    $event->addOperation(
      'contacts_events_accommodation.clone_event_subscriber',
      'cloneAccommodationOperation',
      new TranslatableMarkup('Cloning accommodation'),
    );
  }

  /**
   * Clone operation to clone accommodation.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The new event.
   * @param \Drupal\contacts_events\Entity\EventInterface $source
   *   The source event.
   * @param array $sandbox
   *   A sandbox for storing data that will be preserved between calls to this
   *   operation.
   *
   * @return int
   *   The progress percentage between 0 (not started) and 100 (complete).
   */
  public function cloneAccommodationOperation(EventInterface $event, EventInterface $source, array &$sandbox): int {
    return $this->cloneDependantEntityOperation('c_events_accommodation', $event, $source, $sandbox, 'event');
  }

}

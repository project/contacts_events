<?php

namespace Drupal\contacts_events_accommodation\EventSubscriber;

use Drupal\contacts_events\Event\BookingCompletionValidationEvent;
use Drupal\contacts_events_accommodation\AccommodationHelper;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber for booking completion validation.
 */
class BookingCompletionValidationSubscriber implements EventSubscriberInterface {

  /**
   * The accommodation helper service.
   *
   * @var \Drupal\contacts_events_accommodation\AccommodationHelper
   */
  protected $accommodationHelper;

  /**
   * Construct the subscriber.
   *
   * @param \Drupal\contacts_events_accommodation\AccommodationHelper $accommodation_helper
   *   The accommodation helper service.
   */
  public function __construct(AccommodationHelper $accommodation_helper) {
    $this->accommodationHelper = $accommodation_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[BookingCompletionValidationEvent::NAME][] = 'validateAccommodation';
    return $events;
  }

  /**
   * Validate accommodation totals.
   *
   * @param \Drupal\contacts_events\Event\BookingCompletionValidationEvent $event
   *   The validation event.
   */
  public function validateAccommodation(BookingCompletionValidationEvent $event) {
    $booking_helper = $this->accommodationHelper->getBookingHelper($event->getOrder()->get('order_items'));
    $total_delegates = $booking_helper->getTotalDelegates();
    $accommodation = $booking_helper->getAllAccommodation();
    array_walk($accommodation, function (&$quantity, $key) {
      $quantity = $quantity['total'];
    });
    $result = $this->accommodationHelper->validateAccommodationTotal($total_delegates, $accommodation);

    switch ($result) {
      case AccommodationHelper::VALIDATE_OK:
        // No error, so nothing to do.
        break;

      case AccommodationHelper::VALIDATE_TOO_LITTLE:
        $error = new TranslatableMarkup('It looks like you have not selected enough accommodation for the @count you are booking for.', [
          '@count' => new PluralTranslatableMarkup($total_delegates, 'delegate', '@count delegates'),
        ]);
        break;

      case AccommodationHelper::VALIDATE_TOO_MUCH:
        $error = new TranslatableMarkup('It looks like you have selected too much accommodation for the @count you are booking for.', [
          '@count' => new PluralTranslatableMarkup($total_delegates, 'delegate', '@count delegates'),
        ]);
        break;

      default:
        $error = new TranslatableMarkup('Sorry, we encountered an unknown error.');
        break;
    }

    if (isset($error)) {
      $event->addSoftError($error);
    }
  }

}

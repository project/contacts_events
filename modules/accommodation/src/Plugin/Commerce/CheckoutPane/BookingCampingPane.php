<?php

namespace Drupal\contacts_events_accommodation\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\contacts_events_accommodation\AccommodationHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\ElementSubmit;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the camping pane.
 *
 * @CommerceCheckoutPane(
 *   id = "booking_accom_camping",
 *   label = @Translation("Camping"),
 *   default_step = "accommodation",
 *   wrapper_element = "container",
 *   review_link = @Translation("Manage accommodation")
 * )
 */
class BookingCampingPane extends CheckoutPaneBase {

  /**
   * The accommodation helper service.
   *
   * @var \Drupal\contacts_events_accommodation\AccommodationHelper
   */
  protected $accommodationHelper;

  /**
   * Constructs a new BookingCampingPane object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\contacts_events_accommodation\AccommodationHelper $accommodation_helper
   *   The accommodation helper.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, AccommodationHelper $accommodation_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
    $this->accommodationHelper = $accommodation_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('contacts_events_accommodation.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    /** @var \Drupal\contacts_events\Entity\EventInterface $event */
    $event = $this->order->get('event')->entity;

    // Not visible if there is no accommodation field.
    if (!$event->hasField('accommodation_types')) {
      return FALSE;
    }

    // Visible if camping accommodation is enabled.
    foreach ($event->get('accommodation_types')->getValue() as $item) {
      if ($item['target_id'] == 'camping') {
        return TRUE;
      }
    }

    // Otherwise not visible.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['camping'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'commerce_order',
      '#default_value' => $this->order,
      '#form_mode' => 'booking_accom_camping',
      '#save_entity' => FALSE,
    ];

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $content = [];

    $content['camping'] = [
      '#type' => 'table',
      '#header' => [
        'type' => $this->t('Type'),
        'quantity' => $this->t('Quantity'),
      ],
      '#rows' => [],
      '#empty' => $this->t("You haven't chosen any accommodation yet."),
    ];

    $event = $this->order->get('event')->entity;
    $accommodations = $this->accommodationHelper
      ->getAccommodation($event, 'camping', FALSE);
    $quantities = $this->accommodationHelper
      ->getBookingHelper($this->order->get('order_items'))
      ->getAllAccommodation();
    $marker = '*';
    $has_any_unconfirmed = FALSE;

    foreach ($accommodations as $id => $accommodation) {
      if (isset($quantities[$id])) {
        $has_unconfirmed = $quantities[$id]['total'] != $quantities[$id]['confirmed'];
        if ($has_unconfirmed) {
          $has_any_unconfirmed = TRUE;
        }

        $content['camping']['#rows'][$id] = [
          'type' => $accommodation->label(),
          'quantity' => new FormattableMarkup('@quantity@marker', [
            '@quantity' => $quantities[$id]['total'],
            '@marker' => $has_unconfirmed ? $marker : '',
          ]),
        ];
      }
    }

    if ($has_any_unconfirmed) {
      $content['camping_note'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('@marker Some of this accommodation are awaiting confirmation. Please review and make any required payment to confirm.', [
          '@marker' => $marker,
        ]),
      ];
    }

    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    ElementSubmit::trigger($complete_form, $form_state);
    if (isset($pane_form['#pane_submit'])) {
      foreach ($pane_form['#pane_submit'] as $callback) {
        call_user_func($callback, $complete_form, $form_state);
      }
    }
  }

}

<?php

namespace Drupal\contacts_events_accommodation\Plugin\views\field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Booking Accommodation field handler.
 *
 * @ViewsField("contacts_events_accommodation_booking_accommodation")
 */
class BookingAccommodation extends FieldPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Booking Accommodation Helper.
   *
   * @var \Drupal\contacts_events_accommodation\AccommodationHelper
   */
  protected $accommodationHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->entityTypeManager = $container->get('entity_type.manager');
    $plugin->accommodationHelper = $container->get('contacts_events_accommodation.helper');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function postExecute(&$values) {
    $this->aliases['accommodation'] = $this->field_alias . '__accommodation';
    $this->aliases['pitches'] = $this->field_alias . '__pitches';

    foreach ($values as $row) {
      $booking = $this->getEntity($row);
      $helper = $this->accommodationHelper->getBookingHelper($booking->get('order_items'));

      /** @var \Drupal\contacts_events_accommodation\AccommodationInterface[] $accommodation */
      $accommodation = $this->entityTypeManager
        ->getStorage('c_events_accommodation')
        ->loadMultiple(array_keys($helper->getAllAccommodation()));

      $row->{$this->aliases['accommodation']} = [];
      $row->{$this->aliases['pitches']} = 0;

      foreach ($accommodation as $item) {
        $quantity = $helper->getTotalAccommodation($item->id());
        $row->{$this->aliases['accommodation']}[$item->id()] = [
          'label' => $item->label(),
          'quantity' => $quantity,
        ];

        if ($item->hasField('pitch_size')) {
          $row->{$this->aliases['pitches']} += $item->get('pitch_size')->value * $quantity;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $build['accommodation'] = [
      '#theme' => 'item_list',
      '#items' => [],
    ];

    foreach ($this->getValue($values, 'accommodation') as $item) {
      $build['accommodation']['#items'][] = new FormattableMarkup('@type: @quantity', [
        '@type' => $item['label'],
        '@quantity' => $item['quantity'],
      ]);
    }

    $build['pitch_size']['#markup'] = new FormattableMarkup('<strong>@label@colon</strong> @quantity', [
      '@label' => $this->t('Total pitches'),
      '@colon' => $this->options['element_label_colon'] ? ':' : '',
      '@quantity' => $this->getValue($values, 'pitches'),
    ]);

    // If this field is in a data export, then render it as a single string
    // instead of an HTML list.
    if ($this->view->getDisplay()->getPluginId() == 'data_export') {
      /** @var \Drupal\views_data_export\Plugin\views\display\DataExport $display */
      $display = $this->view->getDisplay();

      if ($display->getContentType() == 'csv') {
        $output = $build['accommodation']['#items'];
        $output[] = $build['pitch_size']['#markup'];
        return implode(', ', $output);
      }
    }

    return $build;
  }

}

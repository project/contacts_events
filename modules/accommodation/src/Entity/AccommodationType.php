<?php

namespace Drupal\contacts_events_accommodation\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Accommodation type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "c_events_accommodation_type",
 *   label = @Translation("Accommodation type"),
 *   label_collection = @Translation("Accommodation types"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\contacts_events_accommodation\Form\AccommodationTypeForm",
 *       "edit" = "Drupal\contacts_events_accommodation\Form\AccommodationTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\contacts_events_accommodation\AccommodationTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer contacts events accommodation types",
 *   bundle_of = "c_events_accommodation",
 *   config_prefix = "c_events_accommodation_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/event-accommodation/add",
 *     "edit-form" = "/admin/structure/event-accommodation/manage/{c_events_accommodation_type}",
 *     "delete-form" = "/admin/structure/event-accommodation/manage/{c_events_accommodation_type}/delete",
 *     "collection" = "/admin/structure/event-accommodation"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class AccommodationType extends ConfigEntityBundleBase {

  /**
   * The machine name of this accommodation type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the accommodation type.
   *
   * @var string
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // See if we have a order item bundle for this type.
    $order_item_type_storage = $this->entityTypeManager()->getStorage('commerce_order_item_type');
    $id = 'ce_accom_' . $this->id();
    $order_item_type = $order_item_type_storage->load($id);

    $label = 'Accommodation: ' . $this->get('label');

    // Create the type if it doesn't exist.
    if (!$order_item_type) {
      $order_item_type_storage
        ->create([
          'id' => 'ce_accom_' . $this->id(),
          'label' => $label,
        ])
        ->save();
    }
    // Otherwise check the label is up to date.
    elseif ($order_item_type->get('label') != $label) {
      $order_item_type
        ->set('label', $label)
        ->save();
    }
  }

}

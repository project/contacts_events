<?php

namespace Drupal\contacts_events_accommodation;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of accommodation type entities.
 *
 * @see \Drupal\contacts_events_accommodation\Entity\AccommodationType
 */
class AccommodationTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No accommodation types available. <a href=":link">Add accommodation type</a>.',
      [':link' => Url::fromRoute('entity.c_events_accommodation_type.add_form')->toString()]
    );

    return $build;
  }

}

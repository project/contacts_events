<?php

namespace Drupal\Tests\contacts_events_accommodation\Unit;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\contacts_events\PriceCalculator;
use Drupal\contacts_events_accommodation\BookingAccommodationHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test the accommodation helper.
 *
 * @group contacts_events
 *
 * @coversDefaultClass \Drupal\contacts_events_accommodation\BookingAccommodationHelper
 */
class BookingAccommodationHelperTest extends UnitTestCase {

  /**
   * Test the calculation of delegates on a booking.
   *
   * @param array $items
   *   An array of order item data. Each item is an array containing:
   *   - bundle: The bundle of the order item.
   *   - state: The state of the order item or NULL if we don't expect to check.
   * @param int $confirmed
   *   The expected confirmed count.
   * @param int $total
   *   The expected total count.
   *
   * @dataProvider dataDelegateCounts
   */
  public function testDelegateCounts(array $items, int $confirmed, int $total) {
    $order_items = [];

    foreach ($items as $item) {
      $order_item = $this->prophesize(OrderItemInterface::class);
      $order_item->bundle()
        ->shouldBeCalledTimes(1)
        ->willReturn($item['bundle']);
      $order_item->get('state')
        ->willReturn((object) ['value' => $item['state']]);
      $order_items[] = $order_item->reveal();
    }

    $item_list = $this->prophesize(EntityReferenceFieldItemListInterface::class);
    $item_list->referencedEntities()
      ->shouldBeCalledTimes(1)
      ->willReturn($order_items);

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $price_calculator = $this->prophesize(PriceCalculator::class);

    $helper = new BookingAccommodationHelper($item_list->reveal(), $entity_type_manager->reveal(), $price_calculator->reveal());
    $this->assertSame($confirmed, $helper->getConfirmedDelegates(), 'Confirmed delegates');
    $this->assertSame($total, $helper->getTotalDelegates(), 'Total delegates');
  }

  /**
   * Data provider for ::testDelegateCounts.
   */
  public function dataDelegateCounts() {
    yield 'no-data' => [
      [],
      0,
      0,
    ];

    yield 'only-one-pending-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
      ],
      0,
      1,
    ];

    yield 'only-multiple-pending-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
      ],
      0,
      3,
    ];

    yield 'only-one-confirmed-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'confirmed'],
      ],
      1,
      1,
    ];

    yield 'only-multiple-confirmed-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'confirmed'],
        ['bundle' => 'contacts_ticket', 'state' => 'confirmed'],
        ['bundle' => 'contacts_ticket', 'state' => 'confirmed'],
      ],
      3,
      3,
    ];

    yield 'only-one-paid-in-full-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'paid_in_full'],
      ],
      1,
      1,
    ];

    yield 'only-multiple-paid-in-full-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'paid_in_full'],
        ['bundle' => 'contacts_ticket', 'state' => 'paid_in_full'],
        ['bundle' => 'contacts_ticket', 'state' => 'paid_in_full'],
      ],
      3,
      3,
    ];

    yield 'only-one-cancelled-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'cancelled'],
      ],
      0,
      0,
    ];

    yield 'only-multiple-cancelled-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'cancelled'],
        ['bundle' => 'contacts_ticket', 'state' => 'cancelled'],
        ['bundle' => 'contacts_ticket', 'state' => 'cancelled'],
      ],
      0,
      0,
    ];
    yield 'only-one-unknown-state-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'other_state'],
      ],
      1,
      1,
    ];

    yield 'multiple-unknown-state-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'team_app_in_progress'],
        ['bundle' => 'contacts_ticket', 'state' => 'other'],
        ['bundle' => 'contacts_ticket', 'state' => 'draft'],
      ],
      3,
      3,
    ];

    yield 'mixed-state-ticket' => [
      [
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
        ['bundle' => 'contacts_ticket', 'state' => 'other_state'],
        ['bundle' => 'contacts_ticket', 'state' => 'confirmed'],
        ['bundle' => 'contacts_ticket', 'state' => 'paid_in_full'],
      ],
      3,
      5,
    ];

    yield 'non-ticket-items' => [
      [
        ['bundle' => 'accom', 'state' => 'pending'],
        ['bundle' => 'other', 'state' => 'pending'],
        ['bundle' => 'ticket', 'state' => 'pending'],
        ['bundle' => 'accom', 'state' => 'other_state'],
        ['bundle' => 'other', 'state' => 'other_state'],
        ['bundle' => 'ticket', 'state' => 'other_state'],
        ['bundle' => 'accom', 'state' => 'confirmed'],
        ['bundle' => 'other', 'state' => 'confirmed'],
        ['bundle' => 'ticket', 'state' => 'confirmed'],
      ],
      0,
      0,
    ];
    yield 'mixed-bundle-mixed-state' => [
      [
        ['bundle' => 'accom', 'state' => 'pending'],
        ['bundle' => 'other', 'state' => 'pending'],
        ['bundle' => 'contacts_ticket', 'state' => 'pending'],
        ['bundle' => 'accom', 'state' => 'other_state'],
        ['bundle' => 'other', 'state' => 'other_state'],
        ['bundle' => 'contacts_ticket', 'state' => 'other_state'],
        ['bundle' => 'accom', 'state' => 'confirmed'],
        ['bundle' => 'other', 'state' => 'confirmed'],
        ['bundle' => 'contacts_ticket', 'state' => 'confirmed'],
      ],
      2,
      3,
    ];
  }

}

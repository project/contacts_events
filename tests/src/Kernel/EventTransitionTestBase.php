<?php

namespace Drupal\Tests\contacts_events\Kernel;

use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Provides a base class for events transition tests.
 */
abstract class EventTransitionTestBase extends EventKernelTestBase {

  /**
   * The events that were triggered by a transition.
   *
   * @var array
   */
  protected $triggeredEvents = [];

  /**
   * The events that were triggered by a transition.
   *
   * @var array
   */
  protected $triggeredEventsByGroup = [];

  /**
   * Get the transition groups for this test.
   *
   * @return array
   *   Array of transition groups.
   */
  protected function getTransitionGroups() {
    return [
      'commerce_order',
      'contacts_events_order_items',
    ];
  }

  /**
   * Get the transitions for this test.
   *
   * @return array
   *   Array of transitions.
   */
  protected function getTransitions() {
    return [
      'place',
      'confirm',
      'confirmed_paid_in_full',
      'paid_in_full',
      'payment_undone',
      'cancel',
    ];
  }

  /**
   * Get the transition phases for this test.
   *
   * @return array
   *   Array of transition phases.
   */
  protected function getTransitionPhases() {
    return [
      'pre_transition',
      'post_transition',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $dispatcher = $this->container->get('event_dispatcher');

    $groups = $this->getTransitionGroups();
    $transitions = $this->getTransitions();
    $phases = $this->getTransitionPhases();

    $events = [];
    $group_events = [];
    foreach ($groups as $group) {
      foreach ($transitions as $transition) {
        foreach ($phases as $phase) {
          $group_events[$group][] = "{$group}.{$transition}.{$phase}";
          $group_events[$group][] = "{$group}.{$phase}";
          $events[] = "state_machine.{$phase}";
        }
      }
    }

    foreach ($events as $event_name) {
      $dispatcher->addListener($event_name, function ($event) use ($event_name) {
        $this->trackEvent($event_name, $event);
      });
    }

    foreach ($group_events as $group_id => $sub_group_events) {
      foreach ($sub_group_events as $event_name) {
        $dispatcher->addListener($event_name, function (WorkflowTransitionEvent $event, $event_name) use ($group_id) {
          $this->trackEvent($event_name, $event, $group_id);
        });
      }
    }
  }

  /**
   * Perform assertions to check the triggered transitions match the expected.
   *
   * @param string $transition
   *   Expected main transition.
   * @param array $additional
   *   Additional expected transitions.
   */
  protected function checkTransitions($transition, array $additional) {
    // Test the main transition events triggered.
    static::assertTrue(in_array("contacts_events_order_items.{$transition}.pre_transition", $this->triggeredEvents), 'Main transition pre_transition did not happen.');
    static::assertTrue(in_array("contacts_events_order_items.{$transition}.post_transition", $this->triggeredEvents), 'Main transition post_transition did not happen.');

    // Test any additional transition events triggered.
    foreach ($additional as $ad_trans) {
      static::assertTrue(in_array("contacts_events_order_items.{$ad_trans}.pre_transition", $this->triggeredEvents), 'Additional transition ' . $ad_trans . ' pre_transition did not happen.');
      static::assertTrue(in_array("contacts_events_order_items.{$ad_trans}.post_transition", $this->triggeredEvents), 'Additional transition ' . $ad_trans . ' post_transition did not happen.');
    }

    // Check that the total count of events matches the expected.
    $transition_count = (int) !empty($transition) + count($additional);
    // Every individual transition has a pre and post event.
    // Also the general group  has a pre and post event.
    /* @see \Drupal\state_machine\Plugin\Field\FieldType\StateItem::dispatchTransitionEvent() */
    $total_event_count = ($transition_count * 2) + 2;
    static::assertEquals($total_event_count, count($this->triggeredEventsByGroup['contacts_events_order_items'] ?? []), 'Total expected event count incorrect.');
  }

  /**
   * Track which transition events have been triggered.
   *
   * @param string $event_name
   *   The name of the triggered event.
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The triggered event.
   * @param string|null $group
   *   (Optional) the event group to easily filter events by.
   */
  public function trackEvent($event_name, WorkflowTransitionEvent $event, $group = NULL) {
    $this->triggeredEvents[$event_name] = $event_name;

    // Also group transitions events so we can count them more easily.
    if ($group) {
      $this->triggeredEventsByGroup[$group][$event_name] = $event_name;
    }
  }

}

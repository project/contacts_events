<?php

namespace Drupal\Tests\contacts_events\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_price\Price;

/**
 * Test the booking process transition events.
 *
 * @group contacts_events
 */
class BookingStateTransitionTest extends EventTransitionTestBase {

  /**
   * Test checks order item transitions for the process of placing an order.
   *
   * @dataProvider dataOnOrderPlaced
   */
  public function testOrderPlacedTransitions($ticket_state_before, $order_status_paid, $ticket_state_after, $transition, $additional, array $payment_values = []) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $order_storage = $entity_type_manager->getStorage('commerce_order');
    $order_item_storage = $entity_type_manager->getStorage('commerce_order_item');

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = Order::create([
      'type' => 'contacts_booking',
      'store_id' => $this->store,
      'state' => 'draft',
      'event' => $this->event->id(),
    ]);
    $order->save();

    // Add our order item.
    $order_item = OrderItem::create([
      'id' => 1,
      'type' => 'contacts_ticket',
      'state' => $ticket_state_before,
      'unit_price' => new Price('10', 'USD'),
      'mapped_price' => [
        'booking_window' => 'standard',
        'class' => 'standard',
      ],
    ]);
    $order_item->save();
    $order->addItem($order_item)->save();

    // Clear any previous events.
    $this->triggeredEvents = [];
    $this->triggeredEventsByGroup = [];

    if ($order_status_paid) {
      Payment::create($payment_values + [
        'type' => 'payment_default',
        'payment_gateway' => $this->gateway->id(),
        'order_id' => $order->id(),
        'state' => 'completed',
        'amount' => new Price('10', 'USD'),
      ])->save();

      // Update order total since payment was created.
      $this->container->get('commerce_payment.order_updater')
        ->updateOrder($order, TRUE);
    }

    // Start transition.
    $order = $order_storage->loadUnchanged($order->id());
    $order->getState()->applyTransitionById('place');
    $order->save();

    if (!empty($transition)) {
      $this->checkTransitions($transition, $additional);
    }
    else {
      static::assertTrue(!isset($this->triggeredEventsByGroup['contacts_events_order_items']), 'No transitions expected.');
    }

    // Check new ticket state.
    $order_item = $order_item_storage->loadUnchanged($order_item->id());
    static::assertEquals($ticket_state_after, $order_item->get('state')->value, 'Order item state does not match expected.');
  }

  /**
   * Data provider for testOrderPlacedTransitions.
   */
  public function dataOnOrderPlaced() {
    $data['pending_not_paid'] = [
      'ticket_state_before' => 'pending',
      'order_status' => FALSE,
      'ticket_state_after' => 'confirmed',
      'transition' => 'confirm',
      'additional' => [],
    ];
    $data['pending_paid'] = [
      'ticket_state_before' => 'pending',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'confirmed_paid_in_full',
      'additional' => ['confirm', 'paid_in_full'],
    ];

    $data['confirmed_not_paid'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => FALSE,
      'ticket_state_after' => 'confirmed',
      'transition' => '',
      'additional' => [],
    ];
    $data['confirmed_paid'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
    ];

    $data['paid_in_full_not_paid'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => FALSE,
      'ticket_state_after' => 'confirmed',
      'transition' => 'payment_undone',
      'additional' => [],
    ];
    $data['paid_in_full_paid'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
    ];

    $data['cancelled_not_paid'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => FALSE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
    ];
    $data['cancelled_paid'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => TRUE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
    ];

    return $data;
  }

  /**
   * Test checks order item transitions for the process of placing an order.
   *
   * @dataProvider dataOnPaymentMade
   */
  public function testPaymentMadeTransitions($ticket_state_before, $order_status_paid, $ticket_state_after, $transition, $additional, array $tracking = NULL) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $order_item_storage = $entity_type_manager->getStorage('commerce_order_item');

    $order_item = OrderItem::create([
      'type' => 'contacts_ticket',
      'state' => $ticket_state_before,
      'unit_price' => new Price('10', $this->store->getDefaultCurrencyCode()),
      'mapped_price' => [
        'booking_window' => 'standard',
        'class' => 'standard',
      ],
    ]);
    $order_item->save();

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = Order::create([
      'type' => 'contacts_booking',
      'store_id' => $this->store,
      'order_items' => [$order_item],
      'state' => 'draft',
      'event' => $this->event->id(),
    ]);
    $order->save();

    $payment_amount = $order->getTotalPrice();
    if ($tracking) {
      $payment_amount = $payment_amount->multiply(0);
      foreach ($tracking as $entry) {
        $payment_amount = $payment_amount->add(new Price($entry['number'], $entry['currency_code']));
      }
    }
    elseif (!$order_status_paid) {
      $payment_amount = $payment_amount->subtract(new Price('1', $payment_amount->getCurrencyCode()));
    }

    // Clear any previous events.
    $this->triggeredEvents = [];
    $this->triggeredEventsByGroup = [];

    // Make the payment.
    Payment::create([
      'type' => 'payment_default',
      'payment_gateway' => $this->gateway->id(),
      'order_id' => $order->id(),
      'state' => 'completed',
      'amount' => $payment_amount,
      'order_item_tracking' => $tracking,
    ])->save();

    // Trigger the order update to apply transitions.
    $this->container->get('commerce_payment.order_updater')->updateOrder($order, TRUE);

    if (!empty($transition)) {
      $this->checkTransitions($transition, $additional);
    }
    else {
      static::assertTrue(!isset($this->triggeredEventsByGroup['contacts_events_order_items']), 'No transitions expected.');
    }

    // Check new ticket state.
    $order_item = $order_item_storage->loadUnchanged($order_item->id());
    static::assertEquals($ticket_state_after, $order_item->get('state')->value, 'Order item state does not match expected.');
  }

  /**
   * Data provider for testPaymentMadeTransitions.
   */
  public function dataOnPaymentMade() {
    $data['pending_not_paid'] = [
      'ticket_state_before' => 'pending',
      'order_status' => FALSE,
      'ticket_state_after' => 'pending',
      'transition' => '',
      'additional' => [],
    ];
    $data['pending_paid'] = [
      'ticket_state_before' => 'pending',
      'order_status' => TRUE,
      'ticket_state_after' => 'pending',
      'transition' => '',
      'additional' => [],
    ];

    $data['confirmed_not_paid'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => FALSE,
      'ticket_state_after' => 'confirmed',
      'transition' => '',
      'additional' => [],
    ];
    $data['confirmed_paid'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
    ];

    $data['paid_in_full_not_paid'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => FALSE,
      'ticket_state_after' => 'confirmed',
      'transition' => 'payment_undone',
      'additional' => [],
    ];
    $data['paid_in_full_paid'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
    ];

    $data['cancelled_not_paid'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => FALSE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
    ];
    $data['cancelled_paid'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => TRUE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
    ];
    return $data;
  }

}

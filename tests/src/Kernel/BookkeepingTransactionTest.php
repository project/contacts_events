<?php

namespace Drupal\Tests\contacts_events\Kernel;

use Drupal\bookkeeping\Entity\Account;
use Drupal\bookkeeping\Entity\AccountInterface;
use Drupal\bookkeeping\Plugin\Field\FieldType\BookkeepingEntryItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\contacts_events\Entity\Event;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\bookkeeping\Kernel\TransactionTrait;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Tests contacts events integration with bookkeeping.
 *
 * @group contacts_events
 */
class BookkeepingTransactionTest extends CommerceKernelTestBase {

  use TransactionTrait;

  /**
   * {@inheritdoc}
   */
  public $strictConfigSchema = FALSE;

  /**
   * The ongoing transaction count.
   *
   * @var int
   */
  protected $transactionCount = 0;

  /**
   * {@inheritdoc}
   *
   * @todo Sort out config so we can remove a lot of these.
   */
  public static $modules = [
    'bookkeeping',
    'dynamic_entity_reference',
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'name',
    'datetime',
    'datetime_range',
    'file',
    'image',
    'commerce_checkout',
    'commerce_order',
    'commerce_payment',
    'contacts',
    'contacts_events',
    'ctools',
    'facets',
    'views',
    'views_data_export',
    'rest',
    'serialization',
  ];

  /**
   * Our sample event.
   *
   * @var \Drupal\contacts_events\Entity\EventInterface
   */
  protected $event;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('bookkeeping_transaction');
    $this->installEntitySchema('contacts_event');
    $this->installConfig('bookkeeping');
    $this->installConfig('commerce_order');
    $this->installConfig('contacts_events');

    // Ensure the bookkeeping config is set up correctly.
    $this->container->get('bookkeeping.commerce_config')->initStore($this->store);

    // Set our storage for the trait.
    $this->transactionStorage = $this->container
      ->get('entity_type.manager')
      ->getStorage('bookkeeping_transaction');

    $this->event = Event::create([
      'type' => 'default',
      'title' => 'Test',
      'code' => 'T',
      'booking_status' => EventInterface::STATUS_OPEN,
    ]);
    $this->event->save();
  }

  /**
   * Test when there is no income field.
   */
  public function testNoIncomeField() {
    // Delete the income account field.
    FieldConfig::loadByName('contacts_event',
      'default',
      'bookkeeping_income_account')
      ->delete();

    $this->doTestIncomeField('commerce_store_' . $this->store->id());
  }

  /**
   * Test when the income field is not set.
   */
  public function testIncomeFieldNotSet() {
    $this->doTestIncomeField('commerce_store_' . $this->store->id());
  }

  /**
   * Test when the income field is set.
   */
  public function testIncomeFieldSet() {
    $account = Account::create([
      'id' => 'event_income',
      'label' => 'Event income',
      'type' => AccountInterface::TYPE_INCOME,
    ]);
    $account->save();
    $this->event
      ->set('bookkeeping_income_account', $account->id())
      ->save();
    $this->doTestIncomeField($account->id());
  }

  /**
   * Run the actual test.
   *
   * @param string $expected_account
   *   The expected income account.
   */
  protected function doTestIncomeField(string $expected_account) {
    // Create the order with a ticket.
    $order_item = OrderItem::create([
      'type' => 'contacts_ticket',
      'unit_price' => new Price('10', 'USD'),
      'mapped_price' => [
        'booking_window' => 'standard',
        'class' => 'standard',
      ],
    ]);
    $order_item->save();
    $order = Order::create([
      'type' => 'contacts_booking',
      'store_id' => $this->store,
      'order_items' => [$order_item],
      'state' => 'draft',
      'event' => $this->event,
    ]);
    $order->save();

    // Check that we have no transactions.
    $this->assertNewTransactionsCount(0, 'Transactions at order creation.');

    // Confirm the booking.
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $order->getState();
    $state->applyTransitionById('place');
    $order->save();

    // Check that we have 1 transactions.
    $this->assertNewTransactionsCount(1, 'Transactions after confirming.');
    $this->assertNewTransactionsDetail([
      [
        'generator' => 'commerce_order_item:payable',
        'entries' => [
          [
            'account' => $expected_account,
            'amount' => 10,
            'currency_code' => 'USD',
            'type' => BookkeepingEntryItem::TYPE_CREDIT,
          ],
          [
            'account' => 'accounts_receivable',
            'amount' => 10,
            'currency_code' => 'USD',
            'type' => BookkeepingEntryItem::TYPE_DEBIT,
          ],
        ],
      ],
    ], 'Transactions after confirming');

    // Add another order item.
    $order_item = OrderItem::create([
      'type' => 'contacts_ticket',
      'unit_price' => new Price('15', 'USD'),
      'mapped_price' => [
        'booking_window' => 'standard',
        'class' => 'standard',
      ],
    ]);
    $order_item->save();
    $order->addItem($order_item);
    $order->save();

    // No new transactions for a pending order item.
    $this->assertNewTransactionsCount(0, 'Transactions after adding pending order item.');

    // Confirm the booking for the changes.
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $order->getState();
    $state->applyTransitionById('place');
    $order->save();

    // Check that we have 1 transactions.
    $this->assertNewTransactionsCount(1, 'Transactions after confirming the new order item.');
    $this->assertNewTransactionsDetail([
      [
        'generator' => 'commerce_order_item:payable',
        'entries' => [
          [
            'account' => $expected_account,
            'amount' => 15,
            'currency_code' => 'USD',
            'type' => BookkeepingEntryItem::TYPE_CREDIT,
          ],
          [
            'account' => 'accounts_receivable',
            'amount' => 15,
            'currency_code' => 'USD',
            'type' => BookkeepingEntryItem::TYPE_DEBIT,
          ],
        ],
      ],
    ], 'Transactions after confirming the new order item');
  }

}

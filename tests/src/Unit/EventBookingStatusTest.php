<?php

namespace Drupal\Tests\contacts_events\Unit;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\contacts_events\Entity\Event;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test event booking status methods.
 *
 * @coversDefaultClass \Drupal\contacts_events\Entity\Event
 * @group contacts_events
 */
class EventBookingStatusTest extends UnitTestCase {

  /**
   * Test checking whether an event has booking enabled.
   *
   * @param string $status
   *   The status of the event.
   * @param bool $expected
   *   The expected return value.
   *
   * @covers ::isBookingEnabled
   * @dataProvider dataIsBookingEnabled
   */
  public function testIsBookingEnabled(string $status, bool $expected) {
    $event = $this->getMockBuilder(Event::class)
      ->setMethods(['get'])
      ->disableOriginalConstructor()
      ->getMock();

    $event->method('get')
      ->with($this->equalTo('booking_status'))
      ->willReturn((object) ['value' => $status]);

    $this->assertSame($expected, $event->isBookingEnabled());
  }

  /**
   * Data provider for ::testIsBookingEnabled.
   */
  public function dataIsBookingEnabled() {
    yield 'disabled' => [
      'status' => EventInterface::STATUS_DISABLED,
      'expected' => FALSE,
    ];

    yield 'closed' => [
      'status' => EventInterface::STATUS_CLOSED,
      'expected' => TRUE,
    ];

    yield 'open' => [
      'status' => EventInterface::STATUS_OPEN,
      'expected' => TRUE,
    ];
  }

  /**
   * Test checking whether an event is open for booking.
   *
   * @param string $status
   *   The status of the event.
   * @param \Drupal\Component\Datetime\DateTimePlus|null $start
   *   The event start date, or NULL for none.
   * @param \Drupal\Component\Datetime\DateTimePlus|null $end
   *   The event end date, or NULL for none.
   * @param bool $expected
   *   The expected return value.
   *
   * @covers ::isBookingOpen
   * @dataProvider dataIsBookingOpen
   */
  public function testIsBookingOpen(string $status, ?DateTimePlus $start, ?DateTimePlus $end, bool $expected) {
    /** @var \PHPUnit_Framework_MockObject_MockObject|\Drupal\contacts_events\Entity\Event $event */
    $event = $this->getMockBuilder(Event::class)
      ->setMethods(['get'])
      ->disableOriginalConstructor()
      ->getMock();

    $event->method('get')
      ->with($this->logicalOr(
        $this->equalTo('booking_status'),
        $this->equalTo('date')
      ))
      ->will($this->returnValueMap([
        ['booking_status', (object) ['value' => $status]],
        [
          'date',
          (object) [
            'start_value' => $start ? $start->getTimestamp() : NULL,
            'start_date' => $start,
            'end_value' => $end ? $end->getTimestamp() : NULL,
            'end_date' => $end,
          ],
        ],
      ]));

    $time = $this->prophesize(TimeInterface::class);
    $time->getRequestTime()
      ->willReturn((new DateTimePlus('2019-07-01'))->getTimestamp());

    $container = $this->prophesize(ContainerInterface::class);
    $container->get('datetime.time')
      ->willReturn($time->reveal());

    \Drupal::setContainer($container->reveal());

    $this->assertSame($expected, $event->isBookingOpen());
  }

  /**
   * Data provider for ::testIsBookingEnabled.
   */
  public function dataIsBookingOpen() {
    yield 'disabled-no-date' => [
      'status' => EventInterface::STATUS_DISABLED,
      'start' => NULL,
      'end' => NULL,
      'expected' => FALSE,
    ];

    yield 'closed-no-date' => [
      'status' => EventInterface::STATUS_CLOSED,
      'start' => NULL,
      'end' => NULL,
      'expected' => FALSE,
    ];

    yield 'open-no-date' => [
      'status' => EventInterface::STATUS_OPEN,
      'start' => NULL,
      'end' => NULL,
      'expected' => TRUE,
    ];

    yield 'disabled-past-start' => [
      'status' => EventInterface::STATUS_DISABLED,
      'start' => new DateTimePlus('2019-01-01'),
      'end' => NULL,
      'expected' => FALSE,
    ];

    yield 'closed-past-start' => [
      'status' => EventInterface::STATUS_CLOSED,
      'start' => new DateTimePlus('2019-01-01'),
      'end' => NULL,
      'expected' => FALSE,
    ];

    yield 'open-past-start' => [
      'status' => EventInterface::STATUS_OPEN,
      'start' => new DateTimePlus('2019-01-01'),
      'end' => NULL,
      'expected' => TRUE,
    ];

    yield 'disabled-future-start' => [
      'status' => EventInterface::STATUS_DISABLED,
      'start' => new DateTimePlus('2020-01-01'),
      'end' => NULL,
      'expected' => FALSE,
    ];

    yield 'closed-future-start' => [
      'status' => EventInterface::STATUS_CLOSED,
      'start' => new DateTimePlus('2020-01-01'),
      'end' => NULL,
      'expected' => FALSE,
    ];

    yield 'open-future-start' => [
      'status' => EventInterface::STATUS_OPEN,
      'start' => new DateTimePlus('2020-01-01'),
      'end' => NULL,
      'expected' => TRUE,
    ];

    yield 'disabled-past-end' => [
      'status' => EventInterface::STATUS_DISABLED,
      'start' => NULL,
      'end' => new DateTimePlus('2019-01-01'),
      'expected' => FALSE,
    ];

    yield 'closed-past-end' => [
      'status' => EventInterface::STATUS_CLOSED,
      'start' => NULL,
      'end' => new DateTimePlus('2019-01-01'),
      'expected' => FALSE,
    ];

    yield 'open-past-end' => [
      'status' => EventInterface::STATUS_OPEN,
      'start' => NULL,
      'end' => new DateTimePlus('2019-01-01'),
      'expected' => FALSE,
    ];

    yield 'disabled-future-end' => [
      'status' => EventInterface::STATUS_DISABLED,
      'start' => NULL,
      'end' => new DateTimePlus('2020-01-01'),
      'expected' => FALSE,
    ];

    yield 'closed-future-end' => [
      'status' => EventInterface::STATUS_CLOSED,
      'start' => NULL,
      'end' => new DateTimePlus('2020-01-01'),
      'expected' => FALSE,
    ];

    yield 'open-future-end' => [
      'status' => EventInterface::STATUS_OPEN,
      'start' => NULL,
      'end' => new DateTimePlus('2020-01-01'),
      'expected' => TRUE,
    ];
  }

}
